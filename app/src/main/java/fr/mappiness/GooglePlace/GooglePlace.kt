package fr.mappiness.GooglePlace

import android.net.Uri
import com.squareup.moshi.Moshi
import fr.mappiness.data.PlaceDetail
import fr.mappiness.data.RepositoryCallback
import fr.mappiness.data.network.RetrofitCallback
import okhttp3.*
import timber.log.Timber
import java.io.IOException

class GooglePlace(
        private val apiKey: String,
        private val httpClient: OkHttpClient,
        private val moshi: Moshi
) {

    private val adapter = moshi.adapter(PlaceDetail::class.java)

    fun detail(placeId: String, callback: RepositoryCallback<PlaceDetail>) {
        val request = Request.Builder()
                .url(detailEndpoint(apiKey, placeId))
                .build()
        httpClient.newCall(request).enqueue(object: Callback{
            override fun onFailure(call: Call, e: IOException) {
                callback.onError(e)
            }
            override fun onResponse(call: Call, response: Response) {
                if (!response.isSuccessful) {
                    callback.onError(IOException("Unexpected code $response"))
                    return
                }
                response.body?.let {
                    callback.onSuccess(adapter.fromJson(it.source()))
                } ?: callback.onError(IOException("Unexpected empty body $response"))
            }
        })
    }

    companion object {
        fun detailEndpoint(apiKey: String, placeId: String): String {
            val url = "https://maps.googleapis.com/maps/api/place/details/json"
            return Uri.parse(url).buildUpon()
                    .appendQueryParameter("key", apiKey)
                    .appendQueryParameter("place_id", placeId)
                    .appendQueryParameter("fields", "formatted_phone_number,opening_hours/weekday_text,opening_hours/open_now,website")
                    .appendQueryParameter("language", "fr")
                    .build()
                    .toString()
        }
    }

}