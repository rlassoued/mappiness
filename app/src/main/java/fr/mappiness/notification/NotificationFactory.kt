package fr.mappiness.notification

import android.Manifest
import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import androidx.core.app.ActivityCompat
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import com.google.firebase.messaging.RemoteMessage
import fr.mappiness.R
import fr.mappiness.data.Pin
import fr.mappiness.ui.HomeActivity
import fr.mappiness.ui.MainActivity
import fr.mappiness.ui.details.PinDetailActivity
import fr.mappiness.utils.getFlags
import fr.mappiness.utils.notificationID
import timber.log.Timber

class NotificationFactory {

    companion object {

        private const val PROXIMITY_NOTIFICATION_CHANNEL_ID = "proximity"
        private const val NEWS_NOTIFICATION_CHANNEL_ID = "news"

        private const val PROXIMITY_NOTIFICATION_GROUP_KEY = "fr.mappiness.PROXIMITY_NOTIFICATION"

        private const val FOREGROUND_NOTIFICATION_ID = 1
        private const val ERROR_NOTIFICATION_ID = 2

        fun showNotification(context: Context, vararg notifications: Pair<Int, Notification>) {
            with(NotificationManagerCompat.from(context)) {
                for (notification in notifications) {
                    Timber.d("Show notification: %d: %s", notification.first, notification.second)
                    if (ActivityCompat.checkSelfPermission(
                            context,
                            Manifest.permission.POST_NOTIFICATIONS
                        ) != PackageManager.PERMISSION_GRANTED
                    ) {
                        // TODO: Consider calling
                        //    ActivityCompat#requestPermissions
                        // here to request the missing permissions, and then overriding
                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                        //                                          int[] grantResults)
                        // to handle the case where the user grants the permission. See the documentation
                        // for ActivityCompat#requestPermissions for more details.
                        return
                    }
                    notify(notification.first, notification.second)
                }
            }
        }

        fun createErrorNotification(
            context: Context,
            title: Int = R.string.error_unknown,
            content: Int = R.string.retry_later
        ): Pair<Int, Notification> {
            registerProximityNotificationChannel(context)
            val intent = Intent(context, HomeActivity::class.java)
            val pendingIntent = PendingIntent.getActivity(context, 0, intent, getFlags(PendingIntent.FLAG_ONE_SHOT))
            val notification =  NotificationCompat.Builder(context, PROXIMITY_NOTIFICATION_CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_stat_status_bar_icon)
                .setContentTitle(context.getString(title))
                .setContentText(context.getString(content))
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setContentIntent(pendingIntent)
                .build()
            return Pair(ERROR_NOTIFICATION_ID, notification)
        }

        fun createForegroundNotification(context: Context): Pair<Int, Notification> {
            registerProximityNotificationChannel(context)
            val homeActivity: PendingIntent =
                Intent(context, HomeActivity::class.java).let { notificationIntent ->
                    PendingIntent.getActivity(context, 20, notificationIntent, getFlags(0))
                }

            val intent = Intent(context, HomeActivity::class.java)
            intent.putExtra("stop",true)
            val homeActivityStopService: PendingIntent =  PendingIntent.getActivity(context, 10, intent, getFlags(0))
            val notification = NotificationCompat.Builder(context, PROXIMITY_NOTIFICATION_CHANNEL_ID)
                .setContentTitle(context.getString(fr.mappiness.R.string.notif_proximity_activated))
                .setContentText(context.getString(fr.mappiness.R.string.notif_proximity_activated_text))
                .setStyle(NotificationCompat.BigTextStyle().bigText(context.getString(fr.mappiness.R.string.notif_proximity_activated_text)))
                .setSmallIcon(R.drawable.ic_stat_status_bar_icon)
                .setContentIntent(homeActivity)
                .addAction(
                    R.drawable.ic_close_black_24dp,
                    context.getString(fr.mappiness.R.string.notif_proximity_action_desactivate),
                    homeActivityStopService
                )
                .build()

            return Pair(FOREGROUND_NOTIFICATION_ID, notification)
        }

        fun createGeofenceNotification(context: Context, pin: Pin): Pair<Int, Notification> {
            registerProximityNotificationChannel(context)
            val intent = Intent(context, PinDetailActivity::class.java).apply {
                putExtra(PinDetailActivity.ARG_PIN_ID, pin.id)
                action = pin.id.toString() // Make sure each intent is treated as a new one
            }
            val pendingIntent = PendingIntent.getActivity(context, 0, intent, getFlags(PendingIntent.FLAG_ONE_SHOT))
            val notification = NotificationCompat.Builder(context, PROXIMITY_NOTIFICATION_CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_stat_status_bar_icon)
                .setContentTitle(pin.name)
                .setContentText(pin.address)
                .setVibrate(longArrayOf(400, 400))
                .setOnlyAlertOnce(true)
                .setAutoCancel(true)
                .setStyle(NotificationCompat.BigTextStyle().bigText("${pin.address}\n ${pin.description}"))
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setContentIntent(pendingIntent)
                .setGroup(PROXIMITY_NOTIFICATION_GROUP_KEY)
                .build()
            return Pair(pin.notificationID(), notification)
        }

        fun createNewsNotification(context: Context, remoteNotification: RemoteMessage.Notification): Pair<Int, Notification> {
            registerNewsNotificationChannel(context)
            val intent = Intent(context, MainActivity::class.java)
            val pendingIntent = PendingIntent.getActivity(context, 0, intent, getFlags(PendingIntent.FLAG_ONE_SHOT))
            val notification = NotificationCompat.Builder(context, NEWS_NOTIFICATION_CHANNEL_ID)
                    .setSmallIcon(R.drawable.ic_stat_status_bar_icon)
                    .setContentTitle(remoteNotification.title)
                    .setContentText(remoteNotification.body)
                    .setVibrate(longArrayOf(400, 400))
                    .setAutoCancel(true)
                    .setStyle(NotificationCompat.BigTextStyle().bigText(remoteNotification.body))
                    .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                    .setContentIntent(pendingIntent)
                    .build()
            return Pair(16, notification)
        }

        /**
         * registerProximityNotificationChannel register the proximity notification channel with the
         * system.
         * Should be called before sending every notification to be sure it is created.
         */
        private fun registerProximityNotificationChannel(context: Context) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                val channel = NotificationChannel(
                        PROXIMITY_NOTIFICATION_CHANNEL_ID,
                        context.getString(R.string.notification_channel_proximity_name),
                        NotificationManager.IMPORTANCE_DEFAULT
                ).apply {
                    description = context.getString(R.string.notification_channel_proximity_description)
                }
                val notificationManager: NotificationManager =
                        context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
                notificationManager.createNotificationChannel(channel)
            }
        }

        /**
         * registerNewsNotificationChannel register the new notification channel with the system.
         * Should be called before sending every notification to be sure it is created.
         */
        private fun registerNewsNotificationChannel(context: Context) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                val channel = NotificationChannel(
                        NEWS_NOTIFICATION_CHANNEL_ID,
                        context.getString(R.string.news_notification_channel_name),
                        NotificationManager.IMPORTANCE_DEFAULT
                ).apply {
                    description = context.getString(R.string.news_notification_channel_description)
                }
                val notificationManager: NotificationManager =
                        context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
                notificationManager.createNotificationChannel(channel)
            }
        }

    }

}