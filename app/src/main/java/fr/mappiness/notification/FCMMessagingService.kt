package fr.mappiness.notification

import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import timber.log.Timber


class FCMMessagingService: FirebaseMessagingService() {

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        super.onMessageReceived(remoteMessage)
        Timber.d("Received notfication: %s", remoteMessage.notification?.title)
        remoteMessage.notification?.let {
            Timber.d("Show notif: %s", it.title)
            val notification = NotificationFactory.createNewsNotification(this, it)
            NotificationFactory.showNotification(this, notification)
        }

    }
}