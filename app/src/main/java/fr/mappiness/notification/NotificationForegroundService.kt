package fr.mappiness.notification

import android.Manifest
import android.app.AlarmManager
import android.app.PendingIntent
import android.app.Service
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.os.Build
import android.os.IBinder
import android.os.SystemClock
import androidx.core.app.ActivityCompat
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.location.*
import fr.mappiness.BuildConfig
import fr.mappiness.data.DataRepository
import fr.mappiness.data.Pin
import fr.mappiness.utils.await
import fr.mappiness.utils.getCurrentLocation
import fr.mappiness.utils.getFlags
import fr.mappiness.utils.toGeofence
import kotlinx.coroutines.*
import kotlinx.coroutines.channels.actor
import org.koin.android.ext.android.inject
import timber.log.Timber
import java.io.IOException

class NotificationForegroundService : Service() {

    private val dataRepository: DataRepository by inject()

    private val fusedLocationClient by lazy { LocationServices.getFusedLocationProviderClient(this) }
    private val geofencingClient by lazy { LocationServices.getGeofencingClient(this) }

    @ObsoleteCoroutinesApi
    private val jobActor = GlobalScope.actor<Action>(Dispatchers.Main) {
        for (action in channel) {
            when (action) {
                is Action.Start -> startMonitoring()
                is Action.GeofenceTrigger -> processGeofenceTrigger(action.intent)
                is Action.Refresh -> refreshMonitoredGeofences()
                else -> {
                    Timber.d("Unknown action")
                }
            }
        }
    }

    private var cachedPins: List<Pin>? = null

    private val locationRequest = LocationRequest.Builder(
        Priority.PRIORITY_HIGH_ACCURACY,
        0
    ).setMinUpdateIntervalMillis(0).build();

    private val refreshGeofenceIntent by lazy {
        PendingIntent.getBroadcast(this, 0, Intent(this, GeofenceRefreshAlarmReceiver::class.java), getFlags(0))
    }

    private val geofencePendingIntent: PendingIntent by lazy {
        PendingIntent.getService(
            this,
            0,
            Intent(this, NotificationForegroundService::class.java).apply {
                putExtra(ARG_ACTION, ACTION_GEOFENCE_TRIGGER)
            }, getFlags(PendingIntent.FLAG_UPDATE_CURRENT)
        )
    }

    override fun onCreate() {
        super.onCreate()
        IS_RUNNING = true
        val notification = NotificationFactory.createForegroundNotification(this)
        startForeground(notification.first, notification.second)
    }

    @ObsoleteCoroutinesApi
    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        val action = intent?.getIntExtra(ARG_ACTION, ACTION_UNDEFINED) ?: ACTION_UNDEFINED
        GlobalScope.launch(Dispatchers.Main) {
            jobActor.send(Action.build(action, intent))
        }
        return START_STICKY
    }

    override fun onBind(intent: Intent?): IBinder? {
        return null
    }

    override fun onDestroy() {
        super.onDestroy()
        IS_RUNNING = false
        stopGeofenceRefreshAlarm()
        removeAllMonitoredGeofences()
    }

    private suspend fun startMonitoring() {
        Timber.d("requestLocationUpdate()")
        try {
            refreshMonitoredGeofences()
            scheduleGeofenceRefreshAlarm()
        } catch (ex: SecurityException) {
            quitWithErrorNotification()
            Timber.e(ex, "NotificationForegroundService failed: no location permission")
        } catch (ex: NullLocationException) {
            quitWithErrorNotification()
            Timber.e(ex, "NotificationForegroundService failed: Location was null")
        } catch (ex: ApiException) {
            quitWithErrorNotification()
            Timber.e(ex, "NotificationForegroundService failed: ApiException: %s", GeofenceStatusCodes.getStatusCodeString(ex.statusCode))
        } catch (ex: Exception) {
            quitWithErrorNotification()
            Timber.e(ex)
        } finally {
            Timber.d("end requestLocationUpdate()")
        }
    }

    private suspend fun getCurrentLocation(): Location {
        val locationResult = GlobalScope.async(Dispatchers.Main) { fusedLocationClient.getCurrentLocation(locationRequest) }.await()
        locationResult ?: throw NullLocationException()
        return locationResult.locations[0]
    }

    private fun getNearestPins(location: Location) = GlobalScope.async {
        dataRepository.listPins(
            query = null,
            lat = location.latitude,
            lng = location.longitude,
            distance = null,
            page = 1,
            perPage = 100
        )
    }

    private fun cachePins(pins: List<Pin>) {
        cachedPins = pins
    }

    private fun retrieveCachedPinsByIDs(ids: List<Long>) = cachedPins?.filter { cached -> ids.any { it == cached.id } }

    private suspend fun replaceMonitoredGeofencesBy(pins: List<Pin>) {
        removeAllMonitoredGeofences()
        val geofences = pins.map { it.toGeofence() }
        val request = GeofencingRequest.Builder()
            .setInitialTrigger(GeofencingRequest.INITIAL_TRIGGER_ENTER)
            .addGeofences(geofences)
            .build()
        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return
        }
        geofencingClient.addGeofences(request, geofencePendingIntent).await()
        cachePins(pins)
    }

    private fun removeAllMonitoredGeofences() {
        try {
            cachedPins?.map { it.id.toString() }?.let {
                if (!it.isEmpty()) {
                    geofencingClient.removeGeofences(it)
                }
            }
            cachedPins = emptyList()
        } catch (e: IllegalArgumentException) {
            Timber.w(e)
        } catch (e: NullPointerException) {
            Timber.w(e)
        } catch (e: NullPointerException) {
            Timber.w(e)
        }
    }

    private fun processGeofenceTrigger(intent: Intent?) {
        val geofencingEvent = intent?.let { GeofencingEvent.fromIntent(it) }
        if (geofencingEvent?.hasError() == true) {
            Timber.e("Error: %s", GeofenceStatusCodes.getStatusCodeString(geofencingEvent.errorCode))
            quitWithErrorNotification()
            return
        }
        val geofences = geofencingEvent?.triggeringGeofences
        val pins = geofences?.let { retrieveCachedPinsByIDs(it.map { it.requestId.toLong() }) }
        if (geofences != null) {
            for (geofence in geofences) {
                Timber.d("Received geofence: %s", geofence)
            }
        }
        for (pin in pins ?: emptyList()) {
            Timber.d("Pin: %s", pin)
        }
        showGeofenceNotifications(pins ?: emptyList())
    }

    private fun showGeofenceNotifications(pins: List<Pin>) {
        val notifications = pins.map { NotificationFactory.createGeofenceNotification(this, it) }
        NotificationFactory.showNotification(this, *notifications.toTypedArray())
    }

    private fun quitWithErrorNotification() {
        NotificationFactory.showNotification(this,
            NotificationFactory.createErrorNotification(this)
        )
        removeAllMonitoredGeofences()
        stopSelf()
    }

    private suspend fun refreshMonitoredGeofences() {
        try {
            val location = getCurrentLocation()
            Timber.d("location is: %s\n", location)
            val pins = getNearestPins(location).await()
            if (pins != null && pins.isNotEmpty()) {
                replaceMonitoredGeofencesBy(pins)
            }
            Timber.d("REFRESH !!")
        } catch (e: IOException) {
            Timber.w("Failed to refresh monitored Geofences", e)
        } catch (e: Exception) {
            Timber.e("Failed to refresh monitored Geofences", e)
        }
    }

    private fun scheduleGeofenceRefreshAlarm() {
        val alarmMgr = getSystemService(Context.ALARM_SERVICE) as AlarmManager
        alarmMgr.setInexactRepeating(
            AlarmManager.ELAPSED_REALTIME_WAKEUP,
            SystemClock.elapsedRealtime() + BuildConfig.MONITORED_GEOFENCE_LIST_REFRESH_INTERVAL,
            BuildConfig.MONITORED_GEOFENCE_LIST_REFRESH_INTERVAL,
            refreshGeofenceIntent
        )
    }

    private fun stopGeofenceRefreshAlarm() {
        val alarmMgr = getSystemService(Context.ALARM_SERVICE) as AlarmManager
        alarmMgr.cancel(refreshGeofenceIntent)
    }

    private class NullLocationException: Exception("Location is null")

    private sealed class Action {
        companion object {
            fun build(action: Int, intent: Intent?) = when(action) {
                    ACTION_START -> Start(action)
                    ACTION_GEOFENCE_TRIGGER -> GeofenceTrigger(action, intent)
                    ACTION_UNDEFINED -> Undefined(action)
                    ACTION_REFRESH -> Refresh(action)
                    else -> Undefined(action)
            }
        }
        data class GeofenceTrigger(val value: Int, val intent: Intent?): Action()
        data class Start(val value: Int): Action()
        data class Undefined(val value: Int): Action()
        data class Refresh(val value: Int): Action()
    }

    companion object {
        const val ARG_ACTION = "action"
        const val ACTION_START: Int = 1
        const val ACTION_GEOFENCE_TRIGGER: Int = 2
        const val ACTION_UNDEFINED: Int = 3
        const val ACTION_REFRESH: Int = 4

        var IS_RUNNING = false
    }
}