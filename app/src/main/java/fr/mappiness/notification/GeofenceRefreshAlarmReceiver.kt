package fr.mappiness.notification

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.Build
import timber.log.Timber

/**
 * Created by loik on 16/06/2017.
 */

class GeofenceRefreshAlarmReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent) {
        Timber.d("Alarm received")
        if (NotificationForegroundService.IS_RUNNING) {
            val newIntent = Intent(context, NotificationForegroundService::class.java)
            newIntent.putExtra(
                NotificationForegroundService.ARG_ACTION,
                NotificationForegroundService.ACTION_REFRESH
            )
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                context.startForegroundService(newIntent)
            } else {
                context.startService(newIntent)
            }
        }
    }

}
