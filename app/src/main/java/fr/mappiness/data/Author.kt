package fr.mappiness.data

import android.os.Parcelable
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
@kotlinx.parcelize.Parcelize
data class Author (
        val id: Long,
        val name: String,
        val picture: String?,
        val bio: String?,
        val link_twitter: String?,
        val link_facebook: String?,
        val link_instagram: String?
): Parcelable
