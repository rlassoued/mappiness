package fr.mappiness.data

import android.os.Parcelable
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import kotlinx.android.parcel.Parcelize
@JsonClass(generateAdapter = true)
data class SyncFilter(
        val tags: List<TagFilter>,
        val emoticons: List<EmoticonFilter>
)

sealed class Filter(
        val type: Int,
        val id: Long,
        var selected: Boolean = false
) {
        companion object {
            const val TYPE_EMOTICON = 0
            const val TYPE_TAG = 1
            const val TYPE_HEADER = 2
        }
}

@JsonClass(generateAdapter = true)
data class EmoticonFilter(
        @Json(name = "id")
        val emoticon: Emoticon
): Filter(TYPE_EMOTICON, emoticon.id)

@JsonClass(generateAdapter = true)
data class TagFilter(
        @Json(name = "id")
        val tagId: Long,
        val name: String
): Filter(TYPE_TAG, tagId)

data class Header(
        val title: String
): Filter(TYPE_HEADER, 0)