package fr.mappiness.data

import android.os.Parcelable
import com.squareup.moshi.FromJson
import com.squareup.moshi.JsonClass
import com.squareup.moshi.ToJson
import fr.mappiness.R
import kotlinx.android.parcel.Parcelize

@kotlinx.parcelize.Parcelize
enum class Category(val id: Long, val drawableIdYellow: Int, val drawableIdGreen: Int, val drawableIdBlue: Int) : Parcelable {
    MONUMENT(1, drawableIdYellow = R.drawable.ic_pin_1_yellow, drawableIdGreen = R.drawable.ic_pin_1_green, drawableIdBlue = R.drawable.ic_pin_1_blue),
    RESTAURANT(2, drawableIdYellow = R.drawable.ic_pin_2_yellow, drawableIdGreen = R.drawable.ic_pin_2_green, drawableIdBlue = R.drawable.ic_pin_2_blue),
    LEGEND(3, drawableIdYellow = R.drawable.ic_pin_3_yellow, drawableIdGreen = R.drawable.ic_pin_3_green, drawableIdBlue = R.drawable.ic_pin_3_blue),
    LIBRARY(4, drawableIdYellow = R.drawable.ic_pin_4_yellow, drawableIdGreen = R.drawable.ic_pin_4_green, drawableIdBlue = R.drawable.ic_pin_4_blue),
    TRANSPORT(5, drawableIdYellow = R.drawable.ic_pin_5_yellow, drawableIdGreen = R.drawable.ic_pin_5_green, drawableIdBlue = R.drawable.ic_pin_5_blue),
    STREET_ART(6, drawableIdYellow = R.drawable.ic_pin_6_yellow, drawableIdGreen = R.drawable.ic_pin_6_green, drawableIdBlue = R.drawable.ic_pin_6_blue),
    PARC(7, drawableIdYellow = R.drawable.ic_pin_7_yellow, drawableIdGreen = R.drawable.ic_pin_7_green, drawableIdBlue = R.drawable.ic_pin_7_blue),
    EDIFICE(8, drawableIdYellow = R.drawable.ic_pin_8_yellow, drawableIdGreen = R.drawable.ic_pin_8_green, drawableIdBlue = R.drawable.ic_pin_8_blue),
    MUSEUM(9, drawableIdYellow = R.drawable.ic_pin_9_yellow, drawableIdGreen = R.drawable.ic_pin_9_green, drawableIdBlue = R.drawable.ic_pin_9_blue),
    SCULPTURE(10, drawableIdYellow = R.drawable.ic_pin_10_yellow, drawableIdGreen = R.drawable.ic_pin_10_green, drawableIdBlue = R.drawable.ic_pin_10_blue),
    BAR(11, drawableIdYellow = R.drawable.ic_pin_11_yellow, drawableIdGreen = R.drawable.ic_pin_11_green, drawableIdBlue = R.drawable.ic_pin_11_blue),
    SPECTACLE(12, drawableIdYellow = R.drawable.ic_pin_12_yellow, drawableIdGreen = R.drawable.ic_pin_12_green, drawableIdBlue = R.drawable.ic_pin_12_blue),
    SHOPPING(13, drawableIdYellow = R.drawable.ic_pin_13_yellow, drawableIdGreen = R.drawable.ic_pin_13_green, drawableIdBlue = R.drawable.ic_pin_13_blue),
    BAKERY(14, drawableIdYellow = R.drawable.ic_pin_14_yellow, drawableIdGreen = R.drawable.ic_pin_14_green, drawableIdBlue = R.drawable.ic_pin_14_blue),
    SPORT(15, drawableIdYellow = R.drawable.ic_pin_15_yellow, drawableIdGreen = R.drawable.ic_pin_15_green, drawableIdBlue = R.drawable.ic_pin_15_blue);

    companion object {
        private val map = values().associateBy(Category::id)
        fun fromLong(type: Long) = map[type]
    }
}

@JsonClass(generateAdapter = true)
@kotlinx.parcelize.Parcelize
data class JSONCategory(val id: Long) : Parcelable

class CategoryAdapter {
    @ToJson
    fun toJson(category: Category): JSONCategory {
        return JSONCategory(category.id)
    }

    @FromJson
    fun fromJson(value: JSONCategory): Category? {
        return Category.fromLong(value.id)
    }
}

//@kotlinx.parcelize.Parcelize
//data class Category (
//        val id: Long,
//        val name: String
////        val icon: CategoryIcon
//): Parcelable