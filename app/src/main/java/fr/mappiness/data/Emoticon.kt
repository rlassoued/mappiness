package fr.mappiness.data

import com.squareup.moshi.FromJson
import com.squareup.moshi.JsonClass
import com.squareup.moshi.ToJson
import fr.mappiness.R

enum class Emoticon(val id: Long, val icon: Int) {
    HEART(1, R.drawable.ic_heart),
    THUMBS_UP(2, R.drawable.ic_thumbs_up),
    CHECKED(3, R.drawable.ic_seen),
    TO_SEE(4, R.drawable.ic_to_see),
    THUMBS_DOWN(5, R.drawable.ic_thumbs_down);

    companion object {
        private val map = Emoticon.values().associateBy(Emoticon::id)
        fun fromLong(type: Long) = map[type]
    }
}

class EmoticonAdapter {
    @ToJson
    fun toJson(emoticon: Emoticon): Long {
        return emoticon.id
    }

    @FromJson
    fun fromJson(value: Long): Emoticon? {
        return Emoticon.fromLong(value)
    }
}