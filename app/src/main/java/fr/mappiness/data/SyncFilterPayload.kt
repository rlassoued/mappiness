package fr.mappiness.data

import android.os.Parcelable
import com.squareup.moshi.JsonClass
import kotlinx.android.parcel.Parcelize

@JsonClass(generateAdapter = true)
@kotlinx.parcelize.Parcelize
data class SyncFilterPayload(val tags: List<Long>, val emoticons: List<Long>) : Parcelable