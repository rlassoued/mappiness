package fr.mappiness.data

import android.os.Parcelable
import com.squareup.moshi.JsonClass
import kotlinx.android.parcel.Parcelize

@JsonClass(generateAdapter = true)
@kotlinx.parcelize.Parcelize
data class Geobook(
    val id: Long,
    val title: String,
    val price: Double?,
    val isFreeOfCharge: Boolean,
    val image: String?,
    val description: String?,
    val isPurchased: Boolean,
    val partnerLogo: String?
) : Parcelable