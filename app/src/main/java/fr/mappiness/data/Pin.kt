package fr.mappiness.data

import android.os.Parcelable
import com.squareup.moshi.JsonClass
import kotlinx.android.parcel.Parcelize

@JsonClass(generateAdapter = true)
@kotlinx.parcelize.Parcelize
data class Pin(
    val id: Long,
    val name: String,
    val description: String,
    val address: String,
    val latitude: Double,
    val longitude: Double,
    val created_at: String,
    val updated_at: String,
    val emoticon: Emoticon?,
    val category: Category,
    val images: List<String>,
    val tags: List<Tag>,
    val distance: Double = -1.0,
    val thumbnail: String?,
    val author: Author,
    val place_id: String,
    val geobooks: List<Geobook>
) : Parcelable