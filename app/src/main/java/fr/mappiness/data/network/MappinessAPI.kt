package fr.mappiness.data.network

import fr.mappiness.data.Geobook
import fr.mappiness.data.Pin
import fr.mappiness.data.PurchasePayload
import fr.mappiness.data.SyncFilter
import fr.mappiness.data.SyncFilterPayload
import okhttp3.MultipartBody
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.DELETE
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Multipart
import retrofit2.http.PATCH
import retrofit2.http.POST
import retrofit2.http.PUT
import retrofit2.http.Part
import retrofit2.http.Path
import retrofit2.http.Query


interface MappinessAPI {

    @Headers("Accept: application/json")
    @GET("/pins")
    fun listPins(
        @Query("query") query: String?,
        @Query("lat") lat: Double?,
        @Query("lng") lng: Double?,
        @Query("distance") distance: Double?,
        @Query("page") page: Int?,
        @Query("per_page") perPage: Int?,
        @Query("lang") lang: String? = "fr"
    ): Call<List<Pin>>

    @Headers("Accept: application/json")
    @GET("/geobooks/pins/{id}/{lang}")
    suspend fun listPins(
        @Path("id") id: Long,
        @Path("lang") lang: String? = "fr"
    ): List<Pin>?

    @Headers("Accept: application/json")
    @GET("/pins/{id}/{lang}")
    fun getPin(@Path("id") query: Long,
               @Path("lang") lang: String? = "fr"): Call<Pin>

    @Headers("Accept: application/json")
    @PATCH("/pins/{id}/emoticon/{emoticon}")
    fun addEmoticon(
        @Path("id") id: Long,
        @Path("emoticon") emoticon: Long
    ): Call<Void>

    @Headers("Accept: application/json")
    @DELETE("/pins/{id}/emoticon")
    fun removeEmoticon(
        @Path("id") id: Long
    ): Call<Void>

    @Headers("Accept: application/json")
    @GET("/filters")
    fun listFilters(): Call<SyncFilter>

    @Headers("Accept: application/json")
    @PUT("/filters")
    fun saveFilters(@Body payload: SyncFilterPayload): Call<Void>

    @Multipart
    @POST("/me/profile_picture")
    fun uploadPicture(@Part picture: MultipartBody.Part): Call<Void>

    @Headers("Accept: application/json")
    @POST("/me/setup")
    fun setupUser(): Call<Void>

    @Headers("Accept: application/json")
    @GET("/geobooks")
    fun listGeobooks(@Query("lang") lang: String? = "fr"): Call<List<Geobook>>

    @Headers("Accept: application/json")
    @POST("/geobooks/purchase")
    fun savePurchase(@Body PurchaseGeobook: PurchasePayload): Call<Void>

}
