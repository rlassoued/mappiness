package fr.mappiness.data.network

import android.content.Context
import android.content.Intent
import com.google.android.gms.tasks.Tasks
import com.google.firebase.FirebaseNetworkException
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseAuthInvalidUserException
import com.google.firebase.auth.GetTokenResult
import fr.mappiness.ui.MainActivity
import okhttp3.Interceptor
import okhttp3.Response
import timber.log.Timber

/**
 * Intercept each request, retrieve the id token from firebase current logged in user and add it to
 * the request.
 */
class FirebaseAuthInterceptor(val context: Context): Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {

        val request = chain.request()

        val user = FirebaseAuth.getInstance().currentUser
        if (user == null) {
            redirectToLogin()
            return chain.proceed(request)
        }

        val token = try {
            Tasks.await(user.getIdToken(true)).token
        } catch (e: FirebaseAuthInvalidUserException) {
            redirectToLogin()
            null
        } catch (e: Exception) {
            Timber.e(e)
            null
        } ?: return chain.proceed(request)
        Timber.d("token auth $token")
        val modifiedRequest = chain.request().newBuilder()
                .addHeader("Authorization", "Bearer $token")
                .build()
        return chain.proceed(modifiedRequest)
    }

    private fun redirectToLogin() {
        val intent = Intent(context, MainActivity::class.java).apply {
            addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        }
        context.startActivity(intent)
    }

}