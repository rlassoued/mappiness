package fr.mappiness.data.network

import fr.mappiness.data.DataRepository
import fr.mappiness.data.Emoticon
import fr.mappiness.data.Geobook
import fr.mappiness.data.Pin
import fr.mappiness.data.PurchasePayload
import fr.mappiness.data.RepositoryCallback
import fr.mappiness.data.SyncFilter
import fr.mappiness.data.SyncFilterPayload
import okhttp3.MultipartBody

class DataRepositoryImpl(private val webservice: MappinessAPI) : DataRepository {

    override fun listPins(
        query: String?,
        lat: Double?,
        lng: Double?,
        distance: Double?,
        page: Int?,
        perPage: Int?,
        callback: RepositoryCallback<List<Pin>>
    ) {
        webservice.listPins(query, lat, lng, distance, page, perPage).enqueue(
            RetrofitCallback(
                callback
            )
        )
    }

    override fun listPins(
        query: String?,
        lat: Double?,
        lng: Double?,
        distance: Double?,
        page: Int?,
        perPage: Int?
    ): List<Pin>? {
        return webservice.listPins(query, lat, lng, distance, page, perPage).execute().body()
    }

    override fun getPinByID(pinId: Long, callback: RepositoryCallback<Pin>) {
        webservice.getPin(pinId).enqueue(RetrofitCallback(callback))
    }

    override fun updatePinEmoticon(pinId: Long, emoticon: Emoticon, callback: RepositoryCallback<Void>) {
        webservice.addEmoticon(pinId, emoticon.id).enqueue(
            RetrofitCallback(
                callback
            )
        )
    }

    override fun removePinEmoticon(pinId: Long, callback: RepositoryCallback<Void>) {
        webservice.removeEmoticon(pinId)
            .enqueue(RetrofitCallback(callback))
    }

    override fun listFilters(callback: RepositoryCallback<SyncFilter>) {
        webservice.listFilters().enqueue(RetrofitCallback(callback))
    }

    override fun saveFilters(
        tags: List<Long>,
        emoticons: List<Long>,
        callback: RepositoryCallback<Void>
    ) {
        webservice.saveFilters(SyncFilterPayload(tags, emoticons)).enqueue(RetrofitCallback(callback))
    }

    override fun uploadPicture(picture: MultipartBody.Part, callback: RepositoryCallback<Void>) {
        webservice.uploadPicture(picture).enqueue(RetrofitCallback(callback))
    }

    override fun setupUser(callback: RepositoryCallback<Void>) {
        webservice.setupUser().enqueue(RetrofitCallback(callback))
    }

    override fun listGeobooks(callback: RepositoryCallback<List<Geobook>>) {
        webservice.listGeobooks().enqueue(RetrofitCallback(callback))
    }

    override suspend fun listPinsByGeobookID(geobookId: Long): List<Pin>? {
        return webservice.listPins(geobookId)
    }

    override fun savePurchase(purchasePayload: PurchasePayload, callback: RepositoryCallback<Void>) {
        webservice.savePurchase(purchasePayload).enqueue(RetrofitCallback(callback))
    }

}