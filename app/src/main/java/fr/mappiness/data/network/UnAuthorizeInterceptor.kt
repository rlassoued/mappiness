package fr.mappiness.data.network

import android.content.Context
import android.content.Intent
import fr.mappiness.ui.MainActivity
import okhttp3.Interceptor
import okhttp3.Response


class UnAuthorizeInterceptor(val context: Context) : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()
        val response = chain.proceed(request)
        if (response.code == 401) {
            redirectToLogin()
            return response
        }
        return response
    }

    private fun redirectToLogin() {
        val intent = Intent(context, MainActivity::class.java).apply {
            flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK
        }
        context.startActivity(intent)
    }

}