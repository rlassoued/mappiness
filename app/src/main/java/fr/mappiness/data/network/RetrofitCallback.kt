package fr.mappiness.data.network

import fr.mappiness.data.FailureStatusCodeException
import fr.mappiness.data.RepositoryCallback
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import timber.log.Timber

class RetrofitCallback<T>(val callback: RepositoryCallback<T>):
    Callback<T> {
    override fun onFailure(call: Call<T>, t: Throwable) {
        Timber.e(t)
        callback.onError(t)
    }

    override fun onResponse(call: Call<T>, response: Response<T>) {
        if (!response.isSuccessful) {
            Timber.w("Request not successful: %d", response.code())
            callback.onError(FailureStatusCodeException(response.code()))
            return
        }
        callback.onSuccess(response.body())
    }
}