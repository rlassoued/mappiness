package fr.mappiness.data

class Ressource<T>(val status: Status, val data: T?, val errorMessage: String?) {

    enum class Status {
        LOADING,
        ERROR,
        SUCCESS
    }

    companion object {
        fun <T> success(data: T?): Ressource<T> {
            return Ressource(Status.SUCCESS, data, null)
        }

        fun <T> error(msg: String): Ressource<T> {
            return Ressource(Status.ERROR, null, msg)
        }

        fun <T> loading(): Ressource<T> {
            return Ressource(Status.LOADING, null, null)
        }
    }

}