package fr.mappiness.data

import android.os.Parcelable
import com.squareup.moshi.JsonClass
import kotlinx.android.parcel.Parcelize

@JsonClass(generateAdapter = true)
@kotlinx.parcelize.Parcelize
data class PlaceDetail(
        val status: String,
        val result: Result?
): Parcelable

@JsonClass(generateAdapter = true)
@kotlinx.parcelize.Parcelize
data class Result(
        val formatted_phone_number: String?,
        val website: String?,
        val opening_hours: OpeningHours?
): Parcelable

@JsonClass(generateAdapter = true)
@kotlinx.parcelize.Parcelize
data class OpeningHours(
        val open_now: Boolean?,
        val weekday_text: List<String>?
): Parcelable