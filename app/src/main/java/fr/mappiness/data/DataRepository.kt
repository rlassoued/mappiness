package fr.mappiness.data

import okhttp3.MultipartBody
import retrofit2.http.Part

interface DataRepository {

    fun listPins(
        query: String?,
        lat: Double?,
        lng: Double?,
        distance: Double?,
        page: Int?,
        perPage: Int?,
        callback: RepositoryCallback<List<Pin>>
    )

    fun listPins(
        query: String?,
        lat: Double?,
        lng: Double?,
        distance: Double?,
        page: Int?,
        perPage: Int?
    ): List<Pin>?

    fun listGeobooks(callback: RepositoryCallback<List<Geobook>>)

    suspend fun listPinsByGeobookID(geobookId: Long) : List<Pin>?

    fun getPinByID(pinId: Long, callback: RepositoryCallback<Pin>)

    fun updatePinEmoticon(pinId: Long, emoticon: Emoticon, callback: RepositoryCallback<Void>)

    fun removePinEmoticon(pinId: Long, callback: RepositoryCallback<Void>)

    fun listFilters(callback: RepositoryCallback<SyncFilter>)

    fun saveFilters(tags: List<Long>, emoticons: List<Long>, callback: RepositoryCallback<Void>)

    fun uploadPicture(@Part picture: MultipartBody.Part, callback: RepositoryCallback<Void>)

    fun setupUser(callback: RepositoryCallback<Void>)

    fun savePurchase(purchasePayload: PurchasePayload, callback: RepositoryCallback<Void>)
}

interface RepositoryCallback<T> {
    fun onSuccess(data: T?)
    fun onError(t: Throwable)
}

class FailureStatusCodeException(val code: Int) : Exception("Request failed $code")