package fr.mappiness

import android.app.Activity
import android.content.Context
import android.content.Intent
import fr.mappiness.ui.MainActivity

interface AuthAware {

    fun redirectUserToLogin(activity: Activity?, context: Context?) {
        val intent = Intent(context, MainActivity::class.java).apply {
            flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK
        }
        activity?.startActivity(intent)
        activity?.finish()
    }

}