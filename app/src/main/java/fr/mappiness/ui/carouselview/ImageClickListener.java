package fr.mappiness.ui.carouselview;

/**
 * Created by leonardo on 06/11/16.
 */

public interface ImageClickListener {
    void onClick(int position);
}
