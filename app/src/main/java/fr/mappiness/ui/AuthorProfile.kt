package fr.mappiness.ui

import android.content.ActivityNotFoundException
import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import com.squareup.picasso.Picasso
import fr.mappiness.R
import fr.mappiness.data.Author
import fr.mappiness.databinding.ActivityAuthorProfileBinding

class AuthorProfile : AppCompatActivity() {

    private lateinit var author: Author
    private lateinit var binding: ActivityAuthorProfileBinding


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityAuthorProfileBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setSupportActionBar(binding.toolbar)
        supportActionBar?.setDisplayShowTitleEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = getString(R.string.contributor)
        title = getString(R.string.contributor)

        author = intent.getParcelableExtra(KEY_AUTHOR)!!

        binding.tvAuthorName.text = author.name
        binding.tvAuthorBio.text = author.bio
        Picasso.Builder(this).build()
            .load(author.picture)
            .placeholder(R.drawable.image_placeholder)
            .error(R.drawable.image_placeholder)
            .fit()
            .centerCrop()
            .into(binding.ivAuthor)
        author.link_facebook?.let { url ->
            if (url.isNotEmpty()) {
                binding.btnFacebook.setOnClickListener { openWebPage(url) }
                binding.btnFacebook.visibility = View.VISIBLE
            }
        }
        author.link_instagram?.let { url ->
            if (url.isNotEmpty()) {
                binding.btnInstagram.setOnClickListener { openWebPage(url) }
                binding.btnInstagram.visibility = View.VISIBLE
            }
        }
        author.link_twitter?.let { url ->
            if (url.isNotEmpty()) {
                binding.btnTwitter.setOnClickListener { openWebPage(url) }
                binding.btnTwitter.visibility = View.VISIBLE
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressedDispatcher.onBackPressed()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    /**
     * Open a web page of a specified URL
     *
     * @param url URL to open
     */
    fun openWebPage(url: String) {
        val uri = Uri.parse(url)
        val intent = Intent(Intent.ACTION_VIEW, uri)
        try {
            startActivity(intent)
        } catch (e: ActivityNotFoundException) {
            Toast.makeText(
                this,
                "No application can handle the link",
                Toast.LENGTH_SHORT
            ).show()
        }
    }

    companion object {
        const val KEY_AUTHOR = "author"
    }
}
