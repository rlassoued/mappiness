package fr.mappiness.ui.filter

import androidx.lifecycle.Observer
import android.os.Bundle
import android.view.*
import android.widget.Toast
import fr.mappiness.R
import fr.mappiness.data.Filter
import fr.mappiness.data.Header
import fr.mappiness.data.Ressource
import fr.mappiness.data.SyncFilter
import fr.mappiness.databinding.FragmentPreferenceDialogBinding
import fr.mappiness.ui.FullScreenDialog
import fr.mappiness.utils.consume
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber
class FilterDialogFragment : FullScreenDialog() {

    private val model: FilterViewModel by viewModel()

    private val tagAdapter = FilterAdapter()

    private lateinit var binding : FragmentPreferenceDialogBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentPreferenceDialogBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.myToolbar.setTitle(R.string.title_preferences_fragment)
        binding.myToolbar.setNavigationIcon(R.drawable.ic_close_white_24dp)
        binding.myToolbar.setNavigationOnClickListener { dismiss() }
        binding.myToolbar.setOnMenuItemClickListener { onMenuItemClick(it) }
        binding.myToolbar.inflateMenu(R.menu.menu_change_password)

        binding.pbFilters.visibility = View.GONE

        binding.rcvTag.apply {
            setHasFixedSize(true)
            adapter = tagAdapter
            layoutManager = androidx.recyclerview.widget.LinearLayoutManager(this@FilterDialogFragment.activity)
        }

        model.filters.observe(this ,Observer {
            when (it?.status) {
                Ressource.Status.LOADING -> showLoaderAndHideList()
                Ressource.Status.ERROR -> showErrorAndHideLoader()
                Ressource.Status.SUCCESS -> showListAndHideLoader(it.data)
                else -> {}
            }
        })

        model.saveAction.observe(this, Observer {
            when (it?.status) {
                Ressource.Status.LOADING -> showLoaderAndHideList()
                Ressource.Status.ERROR -> showErrorAndHideLoader()
                Ressource.Status.SUCCESS -> {
                    (targetFragment as Filterable).onSelectedFilterSave()
                    this.dismiss()
                }
                else -> {}
            }
        })
        model.getFilters()
    }

    private fun onMenuItemClick(item: MenuItem) = when (item.itemId) {
        R.id.action_save ->  consume { model.saveFilters(tagAdapter.selectedTag, tagAdapter.selectedEmoticon) }
        else -> false
    }

    private fun showLoaderAndHideList() {
        binding.pbFilters.visibility = View.VISIBLE
        binding.rcvTag.visibility = View.GONE
    }

    private fun showListAndHideLoader(filters: SyncFilter?) {
        binding.pbFilters.visibility = View.GONE
        binding.rcvTag.visibility = View.VISIBLE
        filters?.let {
            val filters1 = mutableListOf<Filter>().apply {
                add(Header("Tags"))
                addAll(it.tags)
                add(Header("Emoticons"))
                addAll(it.emoticons)
            }
            refreshFilterList(filters1)
        }
    }

    private fun showErrorAndHideLoader() {
        binding.pbFilters.visibility = View.GONE
        Toast.makeText(this.activity, R.string.error_unknown, Toast.LENGTH_LONG).show()
    }

    override fun onResume() {
        val params = dialog?.window?.attributes
        params?.width = WindowManager.LayoutParams.MATCH_PARENT
        params?.height = WindowManager.LayoutParams.MATCH_PARENT
        dialog?.window?.attributes = params as WindowManager.LayoutParams
        super.onResume()
    }

    private fun refreshFilterList(filters: List<Filter>) {
        tagAdapter.submitList(filters)
        Timber.d("Filters: %s\n", filters)
    }

    companion object {
        fun newInstance(): FilterDialogFragment {
            return FilterDialogFragment()
        }
    }
}
