package fr.mappiness.ui.filter

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import fr.mappiness.data.DataRepository
import fr.mappiness.data.RepositoryCallback
import fr.mappiness.data.Ressource
import fr.mappiness.data.SyncFilter
import timber.log.Timber

class FilterViewModel(
    private val dataRepository: DataRepository
): ViewModel() {

    val filters: LiveData<Ressource<SyncFilter>>
        get() = _filters
    private val _filters = MutableLiveData<Ressource<SyncFilter>>()

    val saveAction: LiveData<Ressource<Void>>
        get() = _saveAction
    private val _saveAction = MutableLiveData<Ressource<Void>>()

    fun getFilters() {
        _filters.value = Ressource.loading()
        dataRepository.listFilters(object: RepositoryCallback<SyncFilter> {
            override fun onSuccess(data: SyncFilter?) {
                _filters.value = Ressource.success(data)
            }
            override fun onError(t: Throwable) {
                _filters.value = Ressource.error(t.localizedMessage)
                Timber.e(t)
            }
        })
    }

    fun saveFilters(tags: List<Long>, emoticons: List<Long>) {
        _saveAction.value = Ressource.loading()
        dataRepository.saveFilters(tags, emoticons, object: RepositoryCallback<Void> {
            override fun onSuccess(data: Void?) {
                _saveAction.value = Ressource.success(null)
            }
            override fun onError(t: Throwable) {
                _saveAction.value = Ressource.error(t.localizedMessage)
                Timber.e(t)
            }
        })
    }

}