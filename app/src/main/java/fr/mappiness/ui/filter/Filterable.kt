package fr.mappiness.ui.filter

interface Filterable {

    fun onSelectedFilterSave(): Unit

}