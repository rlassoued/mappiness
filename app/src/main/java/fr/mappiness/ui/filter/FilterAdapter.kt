package fr.mappiness.ui.filter

import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.DiffUtil
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import fr.mappiness.R
import fr.mappiness.data.EmoticonFilter
import fr.mappiness.data.Filter
import fr.mappiness.data.Header
import fr.mappiness.data.TagFilter
import fr.mappiness.databinding.ItemEmoticonSelectorListBinding
import fr.mappiness.databinding.ItemFilterHeaderBinding
import fr.mappiness.databinding.ItemTagSelectorListBinding
import kotlinx.android.extensions.LayoutContainer
/**
 * Created by tran on 15/06/2017.
 */

class FilterAdapter : ListAdapter<Filter, androidx.recyclerview.widget.RecyclerView.ViewHolder>(DIFF_CALLBACK) {

    val selectedTag: List<Long>
        get() = _selectedTag.toList()
    private val _selectedTag = mutableSetOf<Long>()

    val selectedEmoticon: List<Long>
        get() = _selectedEmoticon.toList()
    private val _selectedEmoticon = mutableSetOf<Long>()
    override fun getItemViewType(position: Int) = getItem(position).type

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): androidx.recyclerview.widget.RecyclerView.ViewHolder {
        return when (viewType) {
            Filter.TYPE_TAG -> {
                val view = LayoutInflater.from(parent.context)
                        .inflate(R.layout.item_tag_selector_list, parent, false)
                TagFilterViewHolder(view)
            }
            Filter.TYPE_EMOTICON -> {
                val view = LayoutInflater.from(parent.context)
                        .inflate(R.layout.item_emoticon_selector_list, parent, false)
                EmoticonFilterViewHolder(view)
            }
            else -> {
                val view = LayoutInflater.from(parent.context)
                        .inflate(R.layout.item_filter_header, parent, false)
                HeaderViewHolder(view)
            }
        }
    }

    override fun onBindViewHolder(viewHolder: androidx.recyclerview.widget.RecyclerView.ViewHolder, position: Int) {
        when (viewHolder) {
            is FilterViewHolder -> viewHolder.bind(getItem(position), ::saveState)
            is HeaderViewHolder -> viewHolder.bind(getItem(position))
        }
    }

    override fun submitList(list: List<Filter>?) {
        list?.forEach {
            if (it.selected) {
                when (it.type) {
                    Filter.TYPE_TAG -> if (it.selected) _selectedTag.add(it.id)
                    Filter.TYPE_EMOTICON -> if (it.selected) _selectedEmoticon.add(it.id)
                }
            }
        }
        super.submitList(list)
    }

    private fun saveState(filter: Filter) {
        when (filter.type) {
            Filter.TYPE_TAG -> {
                if (filter.selected)
                    _selectedTag.add(filter.id)
                else
                    _selectedTag.remove(filter.id)
            }
            Filter.TYPE_EMOTICON -> {
                if (filter.selected)
                    _selectedEmoticon.add(filter.id)
                else
                    _selectedEmoticon.remove(filter.id)
            }
        }
    }

    class HeaderViewHolder(override val containerView: View) :
            androidx.recyclerview.widget.RecyclerView.ViewHolder(containerView),
            LayoutContainer
    {
        val binding = ItemFilterHeaderBinding.bind(containerView)
        fun bind(filterHeader: Filter) {
            binding.tvTitle.text = (filterHeader as? Header)?.title
        }

    }

    interface FilterViewHolder {
        fun bind(filter: Filter, onClickCallback: (Filter) -> Unit)
    }

    class TagFilterViewHolder(override val containerView: View) :
            androidx.recyclerview.widget.RecyclerView.ViewHolder(containerView),
            LayoutContainer,
            FilterViewHolder
    {
        val binding = ItemTagSelectorListBinding.bind(containerView)
        override fun bind(filter: Filter, onClickCallback: (Filter) -> Unit) = with(containerView) {
            binding.swtActive.setOnCheckedChangeListener(null)
            val tagFilter = filter as TagFilter
            binding.swtActive.text = tagFilter.name
            binding.swtActive.isChecked = tagFilter.selected
            binding.swtActive.setOnCheckedChangeListener { _, isChecked ->
                tagFilter.selected = isChecked
                onClickCallback(tagFilter)
            }
        }

    }

    class EmoticonFilterViewHolder(override val containerView: View) :
            androidx.recyclerview.widget.RecyclerView.ViewHolder(containerView),
            LayoutContainer,
            FilterViewHolder
    {
        val binding = ItemEmoticonSelectorListBinding.bind(containerView)
        override fun bind(filter: Filter, onClickCallback: (Filter) -> Unit) = with(containerView) {
            binding.swtEmoticonActive.setOnCheckedChangeListener(null)
            val emoticonFilter = filter as EmoticonFilter
            binding.ivEmoticon.setImageResource(emoticonFilter.emoticon.icon)
            binding.swtEmoticonActive.isChecked = emoticonFilter.selected
            binding.swtEmoticonActive.setOnCheckedChangeListener { _, isChecked ->
                emoticonFilter.selected = isChecked
                onClickCallback(emoticonFilter)
            }
        }
    }

    companion object {
        val DIFF_CALLBACK = object : DiffUtil.ItemCallback<Filter>() {
            override fun areItemsTheSame(oldItem: Filter, newItem: Filter): Boolean {
                return oldItem.id == newItem.id && oldItem.type == newItem.type
            }
            override fun areContentsTheSame(oldItem: Filter, newItem: Filter): Boolean {
                return oldItem == newItem
            }
        }
    }

}