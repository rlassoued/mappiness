package fr.mappiness.ui.geobooks

import android.app.Activity
import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.android.billingclient.api.BillingClient
import com.android.billingclient.api.BillingClient.BillingResponseCode.OK
import com.android.billingclient.api.BillingClientStateListener
import com.android.billingclient.api.BillingFlowParams
import com.android.billingclient.api.BillingResult
import com.android.billingclient.api.ConsumeParams
import com.android.billingclient.api.ProductDetailsResponseListener
import com.android.billingclient.api.Purchase
import com.android.billingclient.api.PurchasesUpdatedListener
import com.android.billingclient.api.QueryProductDetailsParams
import com.android.billingclient.api.QueryPurchasesParams
import fr.mappiness.data.DataRepository
import fr.mappiness.data.Geobook
import fr.mappiness.data.PurchasePayload
import fr.mappiness.data.RepositoryCallback

//
// Created by ramzi.lassoued  on 17/10/2020.
//
class GeobookBilling(val context: Context, val dataRepository: DataRepository) {

    private val VERSION_PURCHASE = "001"

    private val billingClient: BillingClient by lazy {
        BillingClient.newBuilder(context)
            .setListener(purchaseUpdaterListener)
            .enablePendingPurchases()
            .build()
    }

    private val _isPurchased: MutableLiveData<Boolean> = MutableLiveData<Boolean>()
    val isPurchased: LiveData<Boolean>
        get() = _isPurchased
    private var geobook: Geobook? = null
    private val purchaseUpdaterListener = PurchasesUpdatedListener { billingResult, purchases ->
        if (billingResult.responseCode == OK && purchases != null) {
            for (purchase in purchases) {
                handlePurchase(purchase, object : RepositoryCallback<Void> {
                    override fun onSuccess(data: Void?) {
                        _isPurchased.value = true
                    }

                    override fun onError(t: Throwable) {
                    }
                })
            }
        }
    }

    fun queryPurchase(_geobook: Geobook) {
        geobook = _geobook
        billingClient.queryPurchasesAsync(
            QueryPurchasesParams.newBuilder().setProductType(BillingClient.ProductType.INAPP)
                .build()
        ) { _, listPurchase ->
            for (purchase in listPurchase) {
                handlePurchase(purchase, object : RepositoryCallback<Void> {
                    override fun onSuccess(data: Void?) {
                    }

                    override fun onError(t: Throwable) {
                    }
                })
            }
        }
    }

    fun startPurchase(activity: Activity, _geobook: Geobook) {
        geobook = _geobook
        billingClient.startConnection(object : BillingClientStateListener {
            override fun onBillingSetupFinished(billingResult: BillingResult) {
                if (billingResult.responseCode == OK) {
                    // The BillingClient is ready. You can query purchases here.
                    queryProductDetails(_geobook) { _, productsDetails ->
                        productsDetails.firstOrNull{ it.equals("${_geobook.id}$VERSION_PURCHASE") }?.let {
                            val productDetailsParamsList =
                                listOf(
                                    BillingFlowParams.ProductDetailsParams.newBuilder()
                                        .setProductDetails(it)
                                        .build()
                                )
                            val billingFlowParams =
                                BillingFlowParams.newBuilder()
                                    .setProductDetailsParamsList(productDetailsParamsList)
                                    .build()
                            billingClient.launchBillingFlow(activity, billingFlowParams)
                        }
                    }
                }
            }

            override fun onBillingServiceDisconnected() {
                // Try to restart the connection on the next request to
                // Google Play by calling the startConnection() method.

            }
        })
    }

    private fun queryProductDetails(
        geobook: Geobook,
        productDetailsResponseListener: ProductDetailsResponseListener
    ) {
        val productList =
            listOf(
                QueryProductDetailsParams.Product.newBuilder()
                    .setProductId("${geobook.id}$VERSION_PURCHASE")
                    .setProductType(BillingClient.ProductType.INAPP)
                    .build()
            )
        val params = QueryProductDetailsParams.newBuilder().setProductList(productList)
        billingClient.queryProductDetailsAsync(params.build(), productDetailsResponseListener)
    }

    private fun handlePurchase(purchase: Purchase, callback: RepositoryCallback<Void>) {
        // Purchase retrieved from BillingClient#queryPurchases or your PurchasesUpdatedListener.
        // Verify the purchase.
        if (purchase.purchaseState == Purchase.PurchaseState.PURCHASED) {
            purchase.products.firstOrNull { it.equals("${geobook?.id}$VERSION_PURCHASE") }?.let{
                if (purchase.purchaseToken.isNotEmpty() && geobook?.isFreeOfCharge == false && geobook?.isPurchased == false) {
                    sendToken(purchase, callback)
                }
                val consumeParams =
                    ConsumeParams.newBuilder()
                        .setPurchaseToken(purchase.purchaseToken)
                        .build()
                billingClient.consumeAsync(consumeParams) { _, _ ->
                }
            }
        }
    }

    private fun sendToken(purchase: Purchase, callback: RepositoryCallback<Void>) {
        geobook?.let {
            val payload = PurchasePayload(
                idGeobook = it.id.toInt(),
                token = purchase.purchaseToken,
                subscriptionId = "${it.id.toInt()}$VERSION_PURCHASE"
            )
            dataRepository.savePurchase(payload, callback)
        }
    }

    fun onCleared() {
        _isPurchased.value = false
    }
}