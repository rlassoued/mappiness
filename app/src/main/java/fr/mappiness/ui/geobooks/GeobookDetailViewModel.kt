package fr.mappiness.ui.geobooks

import android.app.Activity
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import fr.mappiness.data.DataRepository
import fr.mappiness.data.Geobook
import fr.mappiness.data.PurchasePayload
import fr.mappiness.data.RepositoryCallback
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import kotlin.coroutines.CoroutineContext

class GeobookDetailViewModel(
    private val dataRepository: DataRepository,
    private val geobookBilling: GeobookBilling
) : ViewModel(), CoroutineScope {
    // Coroutine's background job
    private val job = Job()

    // Define default thread for Coroutine as Main and add job
    override val coroutineContext: CoroutineContext = Dispatchers.Main + job

    private val _othersGeobooks: MutableLiveData<List<Geobook>> = MutableLiveData()
    val othersGeobooks: LiveData<List<Geobook>>
        get() = _othersGeobooks


    val isPurchased: LiveData<Boolean>
        get() = geobookBilling.isPurchased

    /**
     * load list of others geobooks and set the result in
     * _othersGeobooks for LiveData value for observation.
     */
    fun loadOthersGeobooks(geobook: Geobook) {
        dataRepository.listGeobooks(object : RepositoryCallback<List<Geobook>> {
            override fun onSuccess(data: List<Geobook>?) {
                data?.let {
                    _othersGeobooks.value = data.filterNot { it.id == geobook.id }
                }
            }

            override fun onError(t: Throwable) {}
        })
    }

    fun launchBilling(activity: Activity, geobook: Geobook) {
        geobookBilling.startPurchase(activity, geobook)
    }

    fun queryPurchase(geobook: Geobook) {
        launch { geobookBilling.queryPurchase(geobook) }
    }

    fun launchFreeBilling(geobook: Geobook, callback: RepositoryCallback<Void>) {
        val payload = PurchasePayload(idGeobook = geobook.id.toInt(), null, null)
        dataRepository.savePurchase(payload, callback)
    }

    override fun onCleared() {
        super.onCleared()
        geobookBilling.onCleared()
        // Clear our job when the linked activity is destroyed to avoid memory leaks
        job.cancel()
    }
}