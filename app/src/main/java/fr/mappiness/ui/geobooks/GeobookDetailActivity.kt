package fr.mappiness.ui.geobooks

import android.content.Intent
import android.os.Bundle
import android.view.View.GONE
import android.view.View.INVISIBLE
import android.view.View.VISIBLE
import android.widget.ImageView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import fr.mappiness.R
import fr.mappiness.data.Geobook
import fr.mappiness.data.Pin
import fr.mappiness.data.RepositoryCallback
import fr.mappiness.databinding.ActivityGeobookDetailBinding
import fr.mappiness.ui.SimplePhotoActivity
import fr.mappiness.ui.map.PinDetailsFragment.Companion.ARG_PIN
import fr.mappiness.utils.displayFormatedPrice
import org.koin.androidx.viewmodel.ext.android.viewModel

class GeobookDetailActivity : AppCompatActivity() {


    private val model: GeobookDetailViewModel by viewModel()
    private lateinit var geobook: Geobook
    private var pin: Pin? = null
    private lateinit var binding: ActivityGeobookDetailBinding

    /**
     * Create the adapter and pass the onClickListener that will be attach to each geobook.
     */
    private val mAdapter = GeobooksListAdapter({ geobook: Geobook ->
        val intent = Intent(this, GeobookDetailActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
        intent.putExtra(ARG_GEOBOOK, geobook)
        intent.putExtra(ARG_PIN, pin)
        startActivity(intent)
    }, false)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityGeobookDetailBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setSupportActionBar(binding.toolbar)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        val layoutManager = androidx.recyclerview.widget.LinearLayoutManager(this)
        layoutManager.orientation = RecyclerView.HORIZONTAL
        binding.contentGeobook.listGeobooksFragmentRecyclerView.apply {
            this.setHasFixedSize(true)
            this.adapter = mAdapter
            this.layoutManager = layoutManager
        }

        geobook = intent.getParcelableExtra(ARG_GEOBOOK)!!
        pin = intent.getParcelableExtra(ARG_PIN)
        model.isPurchased.observe(this, { v ->
            if (v) {
                finishAddToMap()
            }
        })
        updateUI()
    }

    override fun onResume() {
        super.onResume()
        model.queryPurchase(geobook)
    }

    private fun finishAddToMap() {
        val alert = AlertDialog.Builder(this)
            .setMessage(getString(if (geobook.isFreeOfCharge) R.string.msg_confirmation_purchase_vert else R.string.msg_confirmation_purchase_blue))
            .setCancelable(false)
            .setPositiveButton(R.string.action_ok) { dialogInterface, _ ->
                dialogInterface.dismiss()
                val intent = Intent().apply {
                    putExtra(ARG_GEOBOOK, geobook.id)
                    putExtra(ARG_PIN, pin)
                }
                setResult(RESULT_OK, intent)
                finish()
            }.create()
        alert.show()
    }

    private fun updateUI() {
        binding.contentGeobook.titleGeobook.text = geobook.title
        binding.contentGeobook.descriptionGeobook.text = geobook.description
        binding.contentGeobook.imageGeobook.setOnClickListener {
            val intent = Intent(this, SimplePhotoActivity::class.java)
            intent.putExtra(SimplePhotoActivity.ARG_PIN_IMAGE, geobook.image)
            startActivity(intent)
        }
        binding.contentGeobook.purchaseGeobook.text =
            getString(if (geobook.isPurchased) R.string.show_pins_geobook else R.string.purchase_geobook)
        binding.contentGeobook.progressPurchaseCircular.visibility = GONE
        binding.contentGeobook.purchaseGeobook.setOnClickListener {
            if (geobook.isPurchased) {
                val intent = Intent().apply {
                    putExtra(ARG_GEOBOOK, geobook.id)
                    putExtra(ARG_PIN, pin)
                }
                setResult(RESULT_OK, intent)
                finish()
            } else if (!geobook.isFreeOfCharge) {
                model.launchBilling(this@GeobookDetailActivity, geobook)
            } else {
                it.apply {
                    isEnabled = false
                    alpha = 0.1f
                }
                binding.contentGeobook.progressPurchaseCircular.visibility = VISIBLE
                model.launchFreeBilling(geobook, object : RepositoryCallback<Void> {
                    override fun onSuccess(data: Void?) {
                        finishAddToMap()
                    }

                    override fun onError(t: Throwable) {
                        binding.contentGeobook.progressPurchaseCircular.visibility = GONE
                        val alert = AlertDialog.Builder(it.context)
                            .setMessage(getString(R.string.error_unknown))
                            .setCancelable(false)
                            .setPositiveButton(R.string.action_ok) { dialogInterface, _ ->
                                dialogInterface.dismiss()
                                it.apply {
                                    isEnabled = true
                                    alpha = 1f
                                }
                            }.create()
                        alert.show()
                    }
                })
            }
        }
        binding.contentGeobook.priceGeobook.visibility = if (geobook.isPurchased) INVISIBLE else VISIBLE
        binding.contentGeobook.priceGeobook.text =
            if (geobook.isFreeOfCharge) getString(R.string.gratuit) else String.format(
                resources.getString(R.string.price), displayFormatedPrice(geobook.price)
            )
        Picasso.Builder(this).build()
            .load(geobook.image)
            .placeholder(R.drawable.image_placeholder)
            .resizeDimen(R.dimen.geobook_width_image_detail, R.dimen.geobook_height_image_detail)
            .error(R.drawable.image_placeholder)
            .into(binding.contentGeobook.imageGeobook as ImageView)
        geobook.partnerLogo?.let { logo ->
            Picasso.Builder(this).build()
                .load(logo)
                .placeholder(R.drawable.image_placeholder)
                .fit()
                .centerCrop()
                .error(R.drawable.image_placeholder)
                .into(binding.contentGeobook.logoBoImage)
        }
        model.othersGeobooks.observe(this, { list ->
            mAdapter.submitList(list)
        })
        model.loadOthersGeobooks(geobook)
        model.queryPurchase(geobook)
    }

    companion object {
        const val ARG_GEOBOOK = "geobook"
    }

}
