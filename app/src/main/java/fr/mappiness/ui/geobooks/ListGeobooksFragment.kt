package fr.mappiness.ui.geobooks

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.SearchView
import androidx.lifecycle.Observer
import fr.mappiness.R
import fr.mappiness.data.Geobook
import fr.mappiness.databinding.FragmentListGeobooksBinding
import fr.mappiness.ui.HomeActivity.Companion.REQUEST_PURCHASE_GEOBOOK
import fr.mappiness.utils.consume
import org.koin.androidx.viewmodel.ext.android.viewModel


class ListGeobooksFragment : androidx.fragment.app.Fragment(), SearchView.OnQueryTextListener {

    private val model: ListGeobooksViewModel by viewModel()
    private lateinit var binding : FragmentListGeobooksBinding

    companion object {
        const val TAG = "geobooks"

        @JvmStatic
        fun newInstance(): ListGeobooksFragment {
            return ListGeobooksFragment()
        }
    }

    /**
     * Create the adapter and pass the onClickListener that will be attach to each geobook.
     */
    private val mAdapter = GeobooksListAdapter({ geobook: Geobook ->
        val intent = Intent(activity, GeobookDetailActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_SINGLE_TOP
        intent.putExtra(GeobookDetailActivity.ARG_GEOBOOK, geobook)
        activity?.startActivityForResult(intent, REQUEST_PURCHASE_GEOBOOK)
    }, true)

    private lateinit var mSearchView: SearchView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.list_geobook_toolbar_actions, menu)
        val searchItem = menu.findItem(R.id.action_search)
        mSearchView = searchItem?.actionView as SearchView
        mSearchView.setOnQueryTextListener(this)
        searchItem.setOnActionExpandListener(object : MenuItem.OnActionExpandListener {
            override fun onMenuItemActionExpand(item: MenuItem): Boolean {
                return true
            }

            override fun onMenuItemActionCollapse(item: MenuItem): Boolean {
                return true
            }
        })
    }

    override fun onQueryTextSubmit(query: String) = consume {
        mAdapter.filter.filter(query)
        mSearchView.clearFocus()
    }

    override fun onQueryTextChange(query: String) = consume {
        mAdapter.filter.filter(query)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        binding = FragmentListGeobooksBinding.inflate(layoutInflater,container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val layoutManager = androidx.recyclerview.widget.GridLayoutManager(this@ListGeobooksFragment.activity, 2)

        binding.listGeobooksFragmentRecyclerView.apply {
            this.setHasFixedSize(true)
            this.adapter = mAdapter
            this.layoutManager = layoutManager
        }

        binding.listGeobooksFragmentProgressbar.visibility = View.GONE

        model.listGeobooks.observe(viewLifecycleOwner, Observer {
            mAdapter.submitList(it)
        })
        model.loadGeobooks()
    }

}