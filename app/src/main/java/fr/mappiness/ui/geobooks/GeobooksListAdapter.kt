package fr.mappiness.ui.geobooks

import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.INVISIBLE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.squareup.picasso.Picasso
import fr.mappiness.R
import fr.mappiness.data.Geobook
import fr.mappiness.databinding.FragmentListGeobooksItemBinding
import fr.mappiness.utils.displayFormatedPrice
import kotlinx.android.extensions.LayoutContainer

class GeobooksListAdapter(private val listener: (Geobook) -> Unit, private val shouldShowPinIcon: Boolean) : ListAdapter<Geobook, GeobooksListAdapter.GeobooksViewHolder>(DIFF_CALLBACK), Filterable {

    lateinit var listGeobook: List<Geobook>
    var listGeobookFiltered: List<Geobook> = emptyList()

    companion object {
        val DIFF_CALLBACK = object : DiffUtil.ItemCallback<Geobook>() {
            override fun areItemsTheSame(oldItem: Geobook, newItem: Geobook): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: Geobook, newItem: Geobook): Boolean {
                return oldItem == newItem
            }
        }
    }

    override fun submitList(list: List<Geobook>?) {
        super.submitList(list)
        if (!::listGeobook.isInitialized) {
            list?.let {
                listGeobook = it
                listGeobookFiltered = it
            }
        }
    }

    override fun getItemCount(): Int {
        return listGeobookFiltered.size
    }

    override fun getItem(position: Int): Geobook {
        return listGeobookFiltered[position]
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GeobooksViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.fragment_list_geobooks_item, parent, false)
        return GeobooksViewHolder(v)
    }

    override fun onBindViewHolder(holder: GeobooksViewHolder, position: Int) {
        holder.bind(getItem(position), listener, shouldShowPinIcon)
    }

    class GeobooksViewHolder(override val containerView: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(containerView), LayoutContainer {
        val binding = FragmentListGeobooksItemBinding.bind(containerView)
        fun bind(geobook: Geobook?, listener: (Geobook) -> Unit, shouldShowPinIcon: Boolean) = with(containerView) {
            binding.geobookImage.visibility = INVISIBLE
            binding.geobookTitle.visibility = INVISIBLE
            binding.geobookDescription.visibility = INVISIBLE
            binding.geobookPrice.visibility = INVISIBLE
            binding.geobookPinIcon.visibility = if (shouldShowPinIcon &&
                (geobook?.isPurchased == true)) VISIBLE else INVISIBLE
            geobook?.let { item ->
                binding.geobookImage.visibility = VISIBLE
                binding.geobookTitle.visibility = VISIBLE
                binding.geobookDescription.visibility = VISIBLE
                binding.geobookPrice.visibility = if (!item.isPurchased) VISIBLE else GONE
                setOnClickListener { listener(item) }
                binding.geobookTitle.text = item.title
                binding.geobookDescription.text = item.description
                binding.geobookPrice.text = if (item.isFreeOfCharge) context.getString(R.string.gratuit) else String.format(resources.getString(R.string.price), displayFormatedPrice(item.price))
                binding.geobookPinIcon.setImageResource(if (item.isFreeOfCharge) R.drawable.ic_pin_green else R.drawable.ic_pin_blue)
                Picasso.Builder(context).build()
                    .load(item.image)
                    .placeholder(R.drawable.image_placeholder)
                    .resizeDimen(R.dimen.geobook_width_image_list, R.dimen.geobook_height_image_list)
                    .error(R.drawable.image_placeholder)
                    .into(binding.geobookImage)

            }
        }

    }

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(charSequence: CharSequence): FilterResults {
                val charString = charSequence.toString()
                listGeobookFiltered = if (charString.isEmpty()) {
                    listGeobook
                } else {
                    val filteredList: MutableList<Geobook> = ArrayList()
                    for (geobook in listGeobook) {
                        if (geobook.title.lowercase().contains(charString.lowercase()) || geobook.description?.lowercase()?.contains(charString.lowercase()) == true) {
                            filteredList.add(geobook)
                        }
                    }
                    filteredList
                }
                val filterResults = FilterResults()
                filterResults.values = listGeobookFiltered
                return filterResults
            }

            override fun publishResults(charSequence: CharSequence, filterResults: FilterResults) {
                listGeobookFiltered = filterResults.values as List<Geobook>
                notifyDataSetChanged()
            }
        }
    }
}