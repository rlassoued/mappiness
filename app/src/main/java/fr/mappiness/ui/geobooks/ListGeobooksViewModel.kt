package fr.mappiness.ui.geobooks

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import fr.mappiness.SharedPrefHelper
import fr.mappiness.data.DataRepository
import fr.mappiness.data.Geobook
import fr.mappiness.data.RepositoryCallback

class ListGeobooksViewModel(private val dataRepository: DataRepository) : ViewModel() {

    private val _listGeobooks: MutableLiveData<List<Geobook>> = MutableLiveData()
    val listGeobooks: LiveData<List<Geobook>>
        get() = _listGeobooks

    /**
     * load list geobooks and set the result in
     * _listGeobooks for LiveData value for observation.
     */
    fun loadGeobooks() {
        dataRepository.listGeobooks(object : RepositoryCallback<List<Geobook>> {
            override fun onSuccess(data: List<Geobook>?) {
                data?.let {
                    _listGeobooks.value = data
                }
            }

            override fun onError(t: Throwable) {}
        })
    }
}