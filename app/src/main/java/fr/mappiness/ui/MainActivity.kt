package fr.mappiness.ui

import android.content.ActivityNotFoundException
import android.content.Intent
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.common.util.IOUtils
import com.google.firebase.auth.FirebaseAuth
import fr.mappiness.SharedPrefHelper
import fr.mappiness.databinding.NoConnectionBinding
import fr.mappiness.ui.auth.LoginActivity
import fr.mappiness.utils.isConnected
import okhttp3.OkHttpClient
import okhttp3.Request
import timber.log.Timber
import java.io.File
import java.io.FileOutputStream
import java.util.concurrent.TimeUnit


class MainActivity : AppCompatActivity() {

    private val mAuth by lazy { FirebaseAuth.getInstance() }

    private lateinit var binding: NoConnectionBinding

    private val REQUEST_CODE_KEY = "request_code"
    private var resultLauncher =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            result.data?.let { data ->
                if ((data.getIntExtra(REQUEST_CODE_KEY, -1) == RC_SIGN_IN)) {
                    if (result.resultCode == RESULT_OK) {
                        val user = FirebaseAuth.getInstance().currentUser
                        Timber.d("User: %s", user)
                        user?.photoUrl?.let {
                            // downloadPicture and then redirect to Home
                            downloadProfilePicture(it.toString()) { redirectToHome() }
                        } ?: redirectToHome()
                    } else if (result.resultCode == RESULT_CANCELED) {
                        finish()
                    }
                }
            }
        }
    companion object {
        const val RC_SIGN_IN = 123
    }

    public override fun onStart() {
        super.onStart()
        if (!isConnected()) {
            binding = NoConnectionBinding.inflate(layoutInflater)
            setContentView(binding.root)
            binding.btnRetry.setOnClickListener {
                if (isConnected()) {
                    startApp()
                }
            }
        } else {
            startApp()
        }
    }

    private fun startApp() {
        mAuth.useAppLanguage()
        // Check if user is signed in (non-null)
        if (mAuth.currentUser == null) {
            requestSignIn()
        } else {
            redirectToHome()
        }
    }

    /**
     * Redirect user to HomeActivity
     */
    private fun redirectToHome() {
        val intent = if (!SharedPrefHelper.getHasDoneInitialSync(this@MainActivity)) {
            Intent(this@MainActivity, HomeActivity::class.java)
        } else {
            Intent(this@MainActivity, HomeActivity::class.java)
        }
        try {
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_SINGLE_TOP
            startActivity(intent)
            finish()
        } catch (ex: ActivityNotFoundException) {
            Timber.e(ex)
        }
    }

    /**
     * Redirect the user to login page for him to login
     */
    private fun requestSignIn() {
        val intent = Intent(this@MainActivity, LoginActivity::class.java)
        try {
            intent.putExtra(REQUEST_CODE_KEY, RC_SIGN_IN)
            resultLauncher.launch(intent)
        } catch (ex: ActivityNotFoundException) {
            Timber.e(ex)
        }
    }

    private fun downloadProfilePicture(url: String, callback: () -> Unit) {
        Timber.d("Download profile picture")
        val runnable = Runnable {
            try {
                val client = OkHttpClient.Builder()
                    .connectTimeout(10, TimeUnit.SECONDS)
                    .readTimeout(15, TimeUnit.SECONDS)
                    .writeTimeout(15, TimeUnit.SECONDS)
                    .build()
                val request = Request.Builder().url(url).build()
                val response = client.newCall(request)
                val input = response.execute().body?.byteStream()
                if (input != null) {
                    val output = FileOutputStream(File(cacheDir, "profile-picture.tmp"), false)
                    IOUtils.copyStream(input, output)
                    input.close()
                    output.close()
                }
            } catch (e: Exception) {
                Timber.e(e)
            } finally {
                runOnUiThread {
                    Timber.d("Finished downloading profile picture")
                    callback()
                }
            }
        }
        try {
            val thread = Thread(runnable)
            thread.start()
        } catch (e: Exception) {
            Timber.e(e)
        }
    }
}
