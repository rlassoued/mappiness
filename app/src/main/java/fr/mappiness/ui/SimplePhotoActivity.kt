package fr.mappiness.ui

import android.os.Bundle
import android.app.Activity
import android.view.View
import com.squareup.picasso.Picasso
import fr.mappiness.R
import fr.mappiness.databinding.ActivitySimplePhotoBinding

class SimplePhotoActivity : Activity(), View.OnClickListener {

    private var mImage: String? = null
    private lateinit var binding: ActivitySimplePhotoBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySimplePhotoBinding.inflate(layoutInflater)
        setContentView(binding.root)

        mImage = intent.getStringExtra(ARG_PIN_IMAGE)

        updateUI()
    }

    fun updateUI() {
        binding.imgButtonClose.setOnClickListener(this)
        Picasso.Builder(this).build()
            .load(mImage)
            .placeholder(R.drawable.image_placeholder)
            .error(R.drawable.image_placeholder)
            .into(binding.zoomImage)
    }

    override fun onClick(v: View?) {
        if (v?.id == binding.imgButtonClose.id) {
            finish()
        }
    }


    companion object {
        const val ARG_PIN_IMAGE = "pin_image"
    }
}
