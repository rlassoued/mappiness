package fr.mappiness.ui.profile

import android.content.ActivityNotFoundException
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.squareup.picasso.Picasso
import fr.mappiness.AuthAware
import fr.mappiness.BuildConfig
import fr.mappiness.R
import fr.mappiness.databinding.FragmentSettingsBinding
import fr.mappiness.ui.AboutActivity

class SettingsFragment :
    Fragment(),
    View.OnClickListener,
    AuthAware {

    private val auth by lazy { FirebaseAuth.getInstance() }

    private lateinit var binding: FragmentSettingsBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentSettingsBinding.inflate(layoutInflater,container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.fragmentSettingsLinearLayoutFirstname.setOnClickListener(this)
        binding.fragmentSettingsTextViewSuggestion.setOnClickListener(this)
        binding.fragmentSettingsTextViewBlog.setOnClickListener(this)
        binding.fragmentSettingsTextViewFacebook.setOnClickListener(this)
        binding.fragmentSettingsTextViewInstagram.setOnClickListener(this)
        binding.fragmentSettingsTextViewAbout.setOnClickListener(this)
        binding.fragmentSettingsTextViewModeEmploi.setOnClickListener(this)
        binding.fragmentSettingsTextViewFaq.setOnClickListener(this)
    }

    override fun onStart() {
        super.onStart()
        auth.currentUser?.let { user ->
            binding.fragmentSettingsTextViewFirstname.text = user.displayName
            Picasso.Builder(requireContext()).build()
                .load(user.photoUrl)
                .placeholder(R.drawable.image_placeholder)
                .error(R.drawable.image_placeholder)
                .fit()
                .centerCrop()
                .into(binding.fragmentSettingsImageViewProfile)
        }
    }

    fun openWebPage(url: String) {
        val uri = Uri.parse(url)
        val intent = Intent(Intent.ACTION_VIEW, uri)
        try {
            startActivity(intent)
        } catch (e: ActivityNotFoundException) {
            Toast.makeText(
                context,
                "No application can handle the link",
                Toast.LENGTH_SHORT
            ).show()
        }
    }

    override fun onClick(v: View) = when (v.id) {
        R.id.fragment_settings_linear_layout_firstname -> onClickProfile(v)
        R.id.fragment_settings_text_view_suggestion -> suggestion(v)
        R.id.fragment_settings_text_view_mode_emploi -> openWebPage(BuildConfig.URL_MODE_EMPLOI)
        R.id.fragment_settings_text_view_faq -> openWebPage(BuildConfig.URL_FAQ)
        R.id.fragment_settings_text_view_blog -> openWebPage(BuildConfig.URL_BLOG)
        R.id.fragment_settings_text_view_facebook -> openWebPage(BuildConfig.URL_FACEBOOK)
        R.id.fragment_settings_text_view_instagram -> openWebPage(BuildConfig.URL_INSTAGRAM)
        R.id.fragment_settings_text_view_about -> about()
        else -> Unit
    }

    private fun onClickProfile(v: View) {
        startActivity(Intent(activity, ProfileActivity::class.java))
    }

    private fun suggestion(v: View) {
        val i = Intent(Intent.ACTION_SENDTO)
        i.type = "message/rfc822"
        i.data = Uri.parse("mailto:")
        i.putExtra(Intent.EXTRA_EMAIL, arrayOf("mariettam@mappiness.fr"))
        i.putExtra(Intent.EXTRA_SUBJECT, "Suggestion de pin")
        try {
            startActivity(i)
        } catch (e: ActivityNotFoundException) {
            Toast.makeText(
                context,
                "No application can handle the link",
                Toast.LENGTH_SHORT
            ).show()
        }
    }

    private fun about() {
        val intent = Intent(context, AboutActivity::class.java)
        startActivity(intent)
    }


    companion object {

        const val TAG = "setting"

        fun newInstance(): SettingsFragment {
            return SettingsFragment()
        }
    }

}
