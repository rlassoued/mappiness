package fr.mappiness.ui.profile

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import fr.mappiness.data.DataRepository
import fr.mappiness.data.RepositoryCallback
import fr.mappiness.data.Ressource
import okhttp3.MultipartBody
import retrofit2.http.Multipart
import timber.log.Timber

class ProfileViewModel(private val dataRepository: DataRepository): ViewModel() {

    val profileURL :LiveData<String>
        get() = _profileURL
    private val _profileURL = MutableLiveData<String>()

    fun uploadProfilePicture(body: MultipartBody.Part) {
        dataRepository.uploadPicture(body, object: RepositoryCallback<Void> {
            override fun onSuccess(data: Void?) {
                _profileURL.value = ""
            }
            override fun onError(t: Throwable) {
//                _profileURL.value = null
                Timber.e(t)
            }
        })
    }

}