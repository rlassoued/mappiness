package fr.mappiness.ui.profile

import android.app.Activity
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import android.view.View
import com.squareup.picasso.Picasso
import fr.mappiness.AuthAware
import fr.mappiness.R
import fr.mappiness.ui.auth.ChangePasswordDialog
import fr.mappiness.ui.auth.DeleteAccountDialog
import android.content.Intent
import androidx.lifecycle.Observer
import android.content.Context
import android.net.Uri
import java.io.File
import androidx.core.content.ContextCompat
import com.facebook.login.LoginManager
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.firebase.auth.*
import com.yalantis.ucrop.UCrop
import fr.mappiness.BuildConfig
import fr.mappiness.databinding.ActivityProfileBinding
import fr.mappiness.utils.currentProviderId
import okhttp3.MediaType
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber

class ProfileActivity :
        AppCompatActivity(),
    View.OnClickListener,
    AuthAware {

    private val auth by lazy { FirebaseAuth.getInstance() }
    private val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(BuildConfig.GOOGLE_OAUTH_TOKEN)
            .requestEmail()
            .build()
    private val googleSignInClient by lazy { GoogleSignIn.getClient(this, gso) }
    val model: ProfileViewModel  by viewModel()

    private var mediaType: MediaType? = null

    private lateinit var binding : ActivityProfileBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityProfileBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setSupportActionBar(binding.toolbar)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        binding.btnDelete.setOnClickListener(this)
        binding.btnLogout.setOnClickListener(this)
        binding.btnChangePassword.setOnClickListener(this)
        binding.ivProfilePicture.setOnClickListener(this)

        if (auth.currentUser?.currentProviderId() != "password") {
            binding.btnChangePassword.isEnabled = false
        }

        model.profileURL.observe(this, Observer {
            auth.currentUser?.reload()?.addOnSuccessListener {
                showUserInfo()
            }?.addOnFailureListener { ex ->
                Timber.w(ex)
            }
        })

        showUserInfo()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            RC_PROFILE_PICTURE -> if (resultCode == Activity.RESULT_OK) cropAndCompressProfilePicture(data) else Timber.w("Pick profile picture cancelled")
            UCrop.REQUEST_CROP -> if (resultCode == Activity.RESULT_OK) uploadProfilePicture(data) else Timber.w("Failed to crop picture")
            else -> super.onActivityResult(requestCode, resultCode, data)
        }
    }

    override fun onClick(v: View) = when (v.id) {
        R.id.btn_change_password -> changePassword(v)
        R.id.btn_delete -> deleteAccount(v)
        R.id.btn_logout -> logOut(v)
        R.id.iv_profile_picture -> pickProfilePicture()
        else -> Unit
    }

    private fun showUserInfo() {
        auth.currentUser?.let { user ->
            binding.tvFirstname.text = user.displayName
            binding.tvEmail.text = user.email
            loadProfilePicture(user.photoUrl)
        }
    }

    private fun loadProfilePicture(uri: Uri?) {
        Picasso.Builder(this).build()
            .load(uri)
                .placeholder(R.drawable.image_placeholder)
                .error(R.drawable.image_placeholder)
                .fit()
                .centerCrop()
                .into(binding.ivProfilePicture)
    }

    private fun logOut(v: View) {
        this.signOut()
        val file = File(this.cacheDir, "profile-picture.tmp")
        if (file.exists()) {
            file.delete()
        }
        redirectUserToLogin(this, this)
    }

    private fun changePassword(v: View) {
        ChangePasswordDialog().show(supportFragmentManager, ChangePasswordDialog.TAG)
    }

    private fun deleteAccount(v: View) = auth.currentUser?.let{ currentUser ->
        val providerId = currentUser.currentProviderId()
        when(providerId) {
            "google.com" -> {
                val sharedPref = getSharedPreferences("auth", Context.MODE_PRIVATE)
                val idToken = sharedPref?.getString("google_id_token", "")
                val cred = GoogleAuthProvider.getCredential(idToken, null)
                this.makeDeleteAccountRequest(currentUser, cred)
            }
            "password" -> {
                DeleteAccountDialog().show(supportFragmentManager, DeleteAccountDialog.TAG)
            }
            "facebook.com" -> {
                val sharedPref = getSharedPreferences("auth", Context.MODE_PRIVATE)
                val idToken = sharedPref?.getString("facebook_id_token", "")
                val cred = FacebookAuthProvider.getCredential(idToken ?: "")
                this.makeDeleteAccountRequest(currentUser, cred)
            }
            else -> {
                Timber.e(Exception("Unknown auth provider id"))
            }
        }
    } ?: Unit

    private fun makeDeleteAccountRequest(user: FirebaseUser, cred: AuthCredential) {
        user.reauthenticateAndRetrieveData(cred)
            .continueWith { it.result?.user?.delete() }
            .addOnSuccessListener(this) {
                this.signOut()
                this.redirectUserToLogin(this, this)
            }
            .addOnFailureListener(this) {
                Timber.e(it)
            }
    }

    private fun signOut() {
        // make specific action base on auth provider
        auth.currentUser?.providerData?.forEach { provider ->
            Timber.d("ProviderId: %s", provider.providerId)
            when (provider.providerId) {
                "google.com" -> googleSignInClient.signOut().addOnFailureListener(this) {
                    Timber.e(it, "Failed to sign out of google account")
                }
                "facebook.com" -> LoginManager.getInstance().logOut()
            }
        }
        auth.signOut()
    }

    private fun pickProfilePicture() {
        val galleryIntent = Intent(
                Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI
        )
        startActivityForResult(galleryIntent, RC_PROFILE_PICTURE)
    }

    private fun cropAndCompressProfilePicture(intent: Intent?) {
        intent?.data?.let {
            val outputFile = File(this.cacheDir, "profile-picture.tmp")
            val outputUri = Uri.parse(outputFile.toURI().toString())
            contentResolver.getType(outputUri)?.let { type ->
                mediaType = type.toMediaTypeOrNull()
            }
            val option = UCrop.Options().apply {
                setCircleDimmedLayer(true)
                setCompressionQuality(50)
                withAspectRatio(1f, 1f)
                setToolbarColor(ContextCompat.getColor(this@ProfileActivity, R.color.colorPrimary))
                withMaxResultSize(200, 200)
            }
            UCrop.of(it, outputUri)
                    .withOptions(option)
                    .start(this)
        }
    }

    private fun uploadProfilePicture(intent: Intent?) {
        if (intent != null && UCrop.getOutput(intent) != null) {
            val outputUri = UCrop.getOutput(intent)!!
            val file = File(outputUri.path)
            val requestFile = RequestBody.create(mediaType, file)
            val body = MultipartBody.Part.createFormData("file", "profile_picture", requestFile)
            model.uploadProfilePicture(body)
        }
    }

    companion object {

        const val RC_PROFILE_PICTURE = 1337

        fun newInstance(): ProfileActivity {
            return ProfileActivity()
        }
    }

}
