package fr.mappiness.ui

import android.location.Location

interface LocationChangedListener {

    fun onLocationChange(location: Location)

    fun onNoLocation()

}