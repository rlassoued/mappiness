package fr.mappiness.ui.auth

import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.google.firebase.auth.*
import com.wajahatkarim3.easyvalidation.core.view_ktx.maxLength
import com.wajahatkarim3.easyvalidation.core.view_ktx.minLength
import com.wajahatkarim3.easyvalidation.core.view_ktx.nonEmpty
import com.wajahatkarim3.easyvalidation.core.view_ktx.textEqualTo
import fr.mappiness.AuthAware
import fr.mappiness.R
import fr.mappiness.databinding.FragmentChangePasswordBinding
import fr.mappiness.ui.FullScreenDialog
import fr.mappiness.utils.consume
import timber.log.Timber

class ChangePasswordDialog : FullScreenDialog(), AuthAware {

    private val auth by lazy { FirebaseAuth.getInstance() }
    private var _binding: FragmentChangePasswordBinding? = null
    private val binding get() = _binding!!
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentChangePasswordBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.toolbar.setNavigationIcon(R.drawable.ic_close_white_24dp)
        binding.toolbar.setNavigationOnClickListener { cancelPasswordUpdate() }
        binding.toolbar.setOnMenuItemClickListener { onMenuItemClick(it) }
        binding.toolbar.inflateMenu(R.menu.menu_change_password)
    }

    private fun onMenuItemClick(item: MenuItem?) = when (item?.itemId) {
        R.id.action_save ->  consume { updatePassword() }
        else -> false
    }

    private fun updatePassword() {
        if (!validateFrom()) {
            return
        }
        auth.currentUser?.let { currentUser ->
            val email = currentUser.email
            if (email != null) {
                val currentPassword = binding.tilCurrentPassword.editText!!.text.toString().trim()
                val newPassword = binding.tilNewPassword.editText!!.text.toString().trim()
                makeChangePasswordRequest(currentUser, email, currentPassword, newPassword)
            } else {
                showFailureMessageAndLogException(Exception("User email is null"))
            }
        }
    }

    private fun validateFrom(): Boolean {
        return  binding.tilCurrentPassword.editText!!.nonEmpty {
            binding.tilCurrentPassword.error = resources.getText(R.string.error_invalid_password)
        }
        && binding.tilNewPassword.editText!!.nonEmpty {
            binding.tilNewPassword.error = resources.getText(R.string.error_validation_password_strenght)
        }
        && binding.tilNewPassword.editText!!.minLength(8) {
            binding.tilNewPassword.error = resources.getText(R.string.error_validation_password_strenght)
        }
        && binding.tilNewPassword.editText!!.maxLength(100) {
            binding.tilNewPassword.error = resources.getText(R.string.error_validation_password_strenght)
        }
        && binding.tilNewPasswordConfirmation.editText!!.textEqualTo(binding.tilNewPassword.editText!!.text.toString().trim()) {
            binding.tilNewPasswordConfirmation.error = getString(R.string.error_confirmation_password_mismatch)
        }
    }

    private fun makeChangePasswordRequest(currentUser: FirebaseUser, email: String, currentPassword: String, newPassword: String) {
        val cred = EmailAuthProvider.getCredential(email, currentPassword)
        currentUser.reauthenticateAndRetrieveData(cred).continueWith {
            it.result?.user?.updatePassword(newPassword)
        }.addOnSuccessListener(requireActivity()) {
            showSuccessMessageAndClose()
        }.addOnFailureListener(requireActivity()) { onRequestFailure(it) }
    }

    private fun onRequestFailure(e: Exception) = when (e) {
        is FirebaseAuthWeakPasswordException -> {
            binding.tilCurrentPassword.error = getString(R.string.error_validation_password_strenght)
        }
        is FirebaseAuthInvalidCredentialsException -> {
            binding.tilCurrentPassword.error = getString(R.string.error_invalid_password)
        }
        is FirebaseAuthInvalidUserException -> {
            auth.signOut()
            showFailureMessageAndLogException(e)
            redirectUserToLogin(activity, context)
        }
        is FirebaseAuthRecentLoginRequiredException -> showFailureMessageAndLogException(e)
        else -> showFailureMessageAndLogException(e)
    }

    private fun cancelPasswordUpdate() {
        dismiss()
    }

    private fun showSuccessMessageAndClose() {
        Toast.makeText(activity, getString(R.string.password_modification_success), Toast.LENGTH_LONG).show()
        dismiss()
    }

    private fun showFailureMessageAndLogException(e: Exception) {
        Timber.e(e)
        Toast.makeText(activity, R.string.error_unknown, Toast.LENGTH_LONG).show()
    }

    companion object {
        const val TAG = "change_password"
    }

}
