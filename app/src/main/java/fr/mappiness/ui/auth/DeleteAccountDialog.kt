package fr.mappiness.ui.auth


import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.google.firebase.auth.*
import fr.mappiness.AuthAware
import fr.mappiness.R
import fr.mappiness.databinding.FragmentDeleteAccountDialogBinding
import fr.mappiness.ui.FullScreenDialog
import fr.mappiness.utils.consume
import timber.log.Timber

class DeleteAccountDialog : FullScreenDialog(), AuthAware {

    private val auth = FirebaseAuth.getInstance()
    private var _binding: FragmentDeleteAccountDialogBinding? = null
    private val binding get() = _binding!!
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentDeleteAccountDialogBinding.inflate(inflater, container, false)
        return binding.root    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.toolbar.setNavigationIcon(R.drawable.ic_close_white_24dp)
        binding.toolbar.setNavigationOnClickListener { cancelAccountDeletion() }
        binding.toolbar.setOnMenuItemClickListener { onMenuItemClick(it) }
        binding.toolbar.inflateMenu(R.menu.menu_delete_account)
    }

    private fun onMenuItemClick(item: MenuItem?) = when (item?.itemId) {
        R.id.action_save ->  consume { deleteAccount() }
        else -> false
    }

    private fun cancelAccountDeletion() {
        dismiss()
    }

    private fun deleteAccount() {
        if (isPasswordEmpty()) {
            binding.tilPassword.error = getString(R.string.error_invalid_password)
        } else {
            auth.currentUser?.let { currentUser ->
                val email = currentUser.email
                if (email != null) {
                    val password = binding.tilPassword.editText!!.text.toString().trim()
                    makeDeleteAccountRequest(currentUser, email, password)
                } else {
                    showFailureMessageAndLogException(Exception("Current user email is null"))
                }
            }
        }
    }

    private fun makeDeleteAccountRequest(currentUser: FirebaseUser, email: String, password: String) {
        val credentials = EmailAuthProvider.getCredential(email, password)
        currentUser.reauthenticateAndRetrieveData(credentials)
            .continueWith { it.result?.user?.delete() }
            .addOnSuccessListener(requireActivity()) {
                auth.signOut()
                redirectUserToLogin(activity, context)
            }
            .addOnFailureListener(requireActivity()) { onRequestFailure(it) }
    }

    private fun onRequestFailure(e: Exception) =  when (e) {
        is FirebaseAuthInvalidCredentialsException -> {
            binding.tilPassword.error = getString(R.string.error_invalid_password)
        }
        is FirebaseAuthInvalidUserException -> {
            auth.signOut()
            showFailureMessageAndLogException(e)
            redirectUserToLogin(activity, context)
        }
        is FirebaseAuthRecentLoginRequiredException -> showFailureMessageAndLogException(e)
        else -> showFailureMessageAndLogException(e)
    }

    private fun showFailureMessageAndLogException(e: Exception) {
        Timber.e(e)
        Toast.makeText(activity, R.string.error_unknown, Toast.LENGTH_LONG).show()
    }

    private fun isPasswordEmpty()= binding.tilPassword.editText!!.text.trim().isEmpty()

    companion object {
        const val TAG = "delete_account"
    }

}
