package fr.mappiness.ui.auth

import android.os.Bundle
import android.text.method.PasswordTransformationMethod
import android.view.View
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.tasks.Task
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException
import com.google.firebase.auth.FirebaseAuthInvalidUserException
import com.google.firebase.auth.FirebaseAuthUserCollisionException
import com.google.firebase.auth.FirebaseAuthWeakPasswordException
import com.google.firebase.auth.UserProfileChangeRequest
import com.wajahatkarim3.easyvalidation.core.view_ktx.atleastOneNumber
import com.wajahatkarim3.easyvalidation.core.view_ktx.atleastOneSpecialCharacters
import com.wajahatkarim3.easyvalidation.core.view_ktx.maxLength
import com.wajahatkarim3.easyvalidation.core.view_ktx.minLength
import com.wajahatkarim3.easyvalidation.core.view_ktx.nonEmpty
import com.wajahatkarim3.easyvalidation.core.view_ktx.validEmail
import fr.mappiness.R
import fr.mappiness.data.DataRepository
import fr.mappiness.data.RepositoryCallback
import fr.mappiness.databinding.ActivityRegisterBinding
import org.koin.android.ext.android.inject
import timber.log.Timber


/**
 * Created by tran on 28/04/2017.
 */

class RegisterActivity : AppCompatActivity() {

    private val mAuth by lazy { FirebaseAuth.getInstance() }
    private val dataRepository: DataRepository by inject()
    private lateinit var binding : ActivityRegisterBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityRegisterBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.passwordToggle.setOnClickListener {
            if (binding.etPassword.transformationMethod == null) {
                binding.etPassword.transformationMethod = PasswordTransformationMethod()
                (it as ImageView).setImageResource(R.drawable.ic_visibility)
            } else {
                binding.etPassword.transformationMethod = null
                (it as ImageView).setImageResource(R.drawable.ic_visibility_off)
            }
            binding.etPassword.setSelection(binding.etPassword.selectionStart, binding.etPassword.selectionEnd)
        }
    }

    fun onClickRegister(v: View) {

        val email = binding.etEmail.text.toString()
        val password = binding.etPassword.text.toString()
        val name = binding.etName.text.toString()

        if (
            email.nonEmpty { binding.etEmail.error = resources.getText(R.string.error_invalid_email) }
            && email.validEmail { binding.etEmail.error = resources.getText(R.string.error_invalid_email) }
            && name.nonEmpty { binding.etName.error = resources.getText(R.string.error_invalid_name) }
            && name.maxLength(100) { binding.etName.error = resources.getText(R.string.error_invalid_name) }
            && password.nonEmpty { binding.etPassword.error = resources.getText(R.string.error_validation_password_strenght) }
            && password.minLength(8) { binding.etPassword.error = resources.getText(R.string.error_validation_password_strenght) }
            && password.maxLength(100) { binding.etPassword.error = resources.getText(R.string.error_validation_password_strenght) }
            && password.atleastOneNumber { binding.etPassword.error = resources.getText(R.string.error_validation_password_strenght) }
            && password.atleastOneSpecialCharacters { binding.etPassword.error = resources.getText(R.string.error_validation_password_strenght) }
        ) {
            signUp(email, password, name)
        }
    }

    fun onClickClose(view: View) {
        setResult(RESULT_CANCELED)
        finish()
    }

    /**
     * Create user account using Firebase Auth
     */
    private fun signUp(email: String, password: String, name: String) {
        mAuth.createUserWithEmailAndPassword(email, password)
            .continueWithTask { task: Task<AuthResult> ->
                val profileUpdates = UserProfileChangeRequest.Builder()
                    .setDisplayName(name)
                    .build()
                task.result?.user?.updateProfile(profileUpdates)
            }
            .addOnSuccessListener(this) {
                mAuth.currentUser?.metadata?.let { metadata ->
                    if (metadata.creationTimestamp == metadata.lastSignInTimestamp) {
                        dataRepository.setupUser(object : RepositoryCallback<Void> {
                            override fun onSuccess(data: Void?) {
                                setResult(RESULT_OK)
                                finish()
                            }

                            override fun onError(t: Throwable) {
                                setResult(RESULT_OK)
                                finish()
                            }
                        })
                    }
                } ?: run {
                    setResult(RESULT_OK)
                    finish()
                }
            }
            .addOnFailureListener(this) { ex: Exception ->
                when (ex) {
                    is FirebaseAuthWeakPasswordException -> {
                        binding.etPassword.error = "Password too short"
                    }
                    is FirebaseAuthInvalidCredentialsException -> {
                        binding.etEmail.error = "Not an email"
                    }
                    is FirebaseAuthUserCollisionException -> {
                        binding.etEmail.error = resources.getText(R.string.error_email_already_used)
                    }
                    is FirebaseAuthInvalidUserException -> {
                        showMessage(R.string.error_unknown, null)
                        Timber.e(ex)
                    }
                    else -> {
                        showMessage(R.string.error_unknown, null)
                        Timber.e(ex)
                    }
                }
            }
    }

    /**
     * showMessage show a message to the user using a snackbar.
     *
     * @param resID Int Text resource id to display
     * @param retryCallback View.OnClickListener
     */
    private fun showMessage(resID: Int, retryCallback: View.OnClickListener?) {
        val msg = Snackbar.make(
            binding.container,
            resID,
            Snackbar.LENGTH_LONG
        )
        retryCallback?.let {
            msg.setAction(R.string.btn_retry, retryCallback)
        }
        msg.show()
    }

}
