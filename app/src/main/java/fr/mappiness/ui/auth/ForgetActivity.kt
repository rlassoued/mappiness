package fr.mappiness.ui.auth

import android.os.Bundle
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.app.AppCompatActivity
import android.view.View
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseAuthInvalidUserException
import com.wajahatkarim3.easyvalidation.core.view_ktx.validEmail
import fr.mappiness.R
import fr.mappiness.databinding.ActivityForgetPasswordBinding
import timber.log.Timber

/**
 * Created by tran on 01/05/2017.
 */

class ForgetActivity : AppCompatActivity() {

    private val mAuth by lazy { FirebaseAuth.getInstance() }
    private lateinit var binding : ActivityForgetPasswordBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityForgetPasswordBinding.inflate(layoutInflater)
        setContentView(binding.root)
    }

    fun onClickSend(v: View) {
        if (!validEmail()) {
            binding.etEmail.error = getString(R.string.error_validation_invalid_email)
        } else {
            sendResetPasswordEmail(binding.etEmail.text.toString())
        }
    }

    /**
     * Send the password reset email if the user is found.
     */
    private fun sendResetPasswordEmail(email: String) {
        mAuth.sendPasswordResetEmail(email)
                .addOnCompleteListener(this) {
                    if (!it.isSuccessful) {
                        when (it.exception) {
                            is FirebaseAuthInvalidUserException -> Timber.w(it.exception)
                            else -> Timber.e(it.exception)
                        }
                    }
                    showMessage(R.string.reset_password_email_sent, null)
                }

    }

    fun onClickClose(v: View) {
        finish()
    }

    private fun validEmail(): Boolean {
        return binding.etEmail.validEmail()
    }

    /**
     * showMessage show a message to the user using a snackbar.
     *
     * @param resID Int Text resource id to display
     * @param retryCallback View.OnClickListener
     */
    private fun showMessage(resID: Int, retryCallback: View.OnClickListener?) {
        val msg = Snackbar.make(
                binding.content,
                resID,
                Snackbar.LENGTH_LONG
        )
        retryCallback?.let {
            msg.setAction(R.string.btn_retry, retryCallback)
        }
        msg.show()
    }
}
