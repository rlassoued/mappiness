package fr.mappiness.ui.auth

import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.facebook.AccessToken
import com.facebook.CallbackManager
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FacebookAuthProvider
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException
import com.google.firebase.auth.FirebaseAuthInvalidUserException
import com.google.firebase.auth.FirebaseAuthUserCollisionException
import com.google.firebase.auth.GoogleAuthProvider
import com.wajahatkarim3.easyvalidation.core.view_ktx.nonEmpty
import com.wajahatkarim3.easyvalidation.core.view_ktx.validEmail
import fr.mappiness.BuildConfig
import fr.mappiness.R
import fr.mappiness.data.DataRepository
import fr.mappiness.data.RepositoryCallback
import fr.mappiness.databinding.ActivityLoginBinding
import org.koin.android.ext.android.inject
import timber.log.Timber

class LoginActivity : AppCompatActivity() {

    private val mAuth by lazy { FirebaseAuth.getInstance() }
    private val dataRepository: DataRepository by inject()

    private lateinit var binding : ActivityLoginBinding

    private val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
        .requestIdToken(BuildConfig.GOOGLE_OAUTH_TOKEN)
        .requestEmail()
        .build()
    private val googleSignInClient by lazy { GoogleSignIn.getClient(this, gso) }

    private val callbackManager = CallbackManager.Factory.create()
    private val fbLoginManager = LoginManager.getInstance()

    companion object {
        const val RC_SIGN_UP = 987
        const val RC_GOOGLE_SIGN_IN = 988
    }

    private val isOnline: Boolean
        get() {
            val cm = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            return cm.activeNetworkInfo != null && cm.activeNetworkInfo!!.isConnectedOrConnecting
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.pbLogin.visibility = View.INVISIBLE

        binding.btnGoogleSignin.setOnClickListener {
            startGoogleSignInActivity()
        }

        fbLoginManager.registerCallback(callbackManager, object : FacebookCallback<LoginResult> {
            override fun onSuccess(loginResult: LoginResult) {
                Timber.d("facebook:onSuccess:$loginResult")
                handleFacebookAccessToken(loginResult.accessToken)
            }

            override fun onCancel() {
                Timber.d("facebook:onCancel")
                // ...
            }

            override fun onError(error: FacebookException) {
                showFailureMessageAndLogException(error)
            }
        })

        binding.btnFacebookSignin.setOnClickListener {
            fbLoginManager.logInWithReadPermissions(this, listOf("email", "public_profile"))
        }

        if (BuildConfig.DEBUG) {
            binding.etEmail.setText("test@test.com")
            binding.etPassword.setText("testtest")
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        callbackManager.onActivityResult(requestCode, resultCode, data)
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            // If successfully signUp return the result SUCCESS
            RC_SIGN_UP -> if (resultCode == AppCompatActivity.RESULT_OK) {
                setResult(AppCompatActivity.RESULT_OK)
                finish()
            }
            RC_GOOGLE_SIGN_IN -> if (resultCode == AppCompatActivity.RESULT_OK) {
                val task = GoogleSignIn.getSignedInAccountFromIntent(data)
                try {
                    val account = task.getResult(ApiException::class.java)
                    signInAuthWithGoogle(account!!)
                } catch (e: ApiException) {
                    showFailureMessageAndLogException(e)
                }
            } else {
                try {
                    GoogleSignIn.getSignedInAccountFromIntent(data).getResult(ApiException::class.java)
                } catch (e: ApiException) {
                    showFailureMessageAndLogException(e)
                }
            }
            else -> Timber.w("Unknown activity result %d: %d\n %s", requestCode, resultCode, data)
        }
    }

    /**
     * Respond to onclick event on CGU and privacy policy button
     * @param view the view triggering the click event
     */
    fun onBtnClick(view: View) {
        when (view.id) {
            R.id.btn_cgu -> openWebPage(BuildConfig.URL_CGU)
        }
    }

    /**
     * Called when the user click the connection button
     *
     * @param v View is the view that triggered the event
     */
    fun onClickConnect(v: View) {
        if (!validateInput()) {
            return
        }
        val email = binding.etEmail.text.toString().trim()
        val password = binding.etPassword.text.toString().trim()
        if (isOnline) {
            hideButtonsAndShowLoader()
            signInWithEmailPassword(email, password)
        } else {
            showNoConnectionMessage {
                signInWithEmailPassword(binding.etEmail.text.toString(), binding.etPassword.text.toString())
            }
        }
    }

    fun onClickRegister(v: View) {
        val intent = Intent(this@LoginActivity, RegisterActivity::class.java)
        startActivityForResult(intent, RC_SIGN_UP)
    }

    fun onClickForget(v: View) {
        val intent = Intent(this@LoginActivity, ForgetActivity::class.java)
        startActivity(intent)
    }

    /**
     * showNoConnectionMessage show a message to let the user know the internet connection
     * is not available. Take View.OnClickListener run when the user click the retry button.
     *
     * @param retryCallback View.OnClickListener
     */
    private fun showNoConnectionMessage(retryCallback: (View) -> Unit) {
        Snackbar.make(
            findViewById(R.id.snackbar_container),
            R.string.text_no_connection,
            Snackbar.LENGTH_INDEFINITE
        ).apply {
            setAction(R.string.btn_retry, retryCallback)
            show()
        }
    }

    /**
     * signInWithEmailPassword sign in the user with email and password with Firebase
     */
    private fun signInWithEmailPassword(email: String, password: String) {
        mAuth.signInWithEmailAndPassword(email, password)
            // On successful sign in return SUCCESS
            .addOnSuccessListener(this) { _: AuthResult ->
                setResult(AppCompatActivity.RESULT_OK)
                finish()
            }
            // On failure display error messages
            .addOnFailureListener(this) { e: Exception ->
                showButtonsAndHideLoader()
                when (e) {
                    is FirebaseAuthInvalidUserException -> {
                        binding.etEmail.error = resources.getText(R.string.error_wrong_passwd_or_email)
                        binding.etPassword.error = resources.getText(R.string.error_wrong_passwd_or_email)
                    }
                    is FirebaseAuthInvalidCredentialsException -> {
                        binding.etEmail.error = resources.getText(R.string.error_wrong_passwd_or_email)
                        binding.etPassword.error = resources.getText(R.string.error_wrong_passwd_or_email)
                    }
                    else -> showFailureMessageAndLogException(e)
                }
            }

    }

    /**
     * Start Google sign in Activity. We get the result in onActivityResult
     */
    private fun startGoogleSignInActivity() {
        val signInIntent = googleSignInClient.signInIntent
        startActivityForResult(signInIntent, RC_GOOGLE_SIGN_IN)
    }

    private fun signInAuthWithGoogle(acct: GoogleSignInAccount) {
        val credential = GoogleAuthProvider.getCredential(acct.idToken, null)
        val sharedPref = this.getSharedPreferences("auth", Context.MODE_PRIVATE) ?: return
        with(sharedPref.edit()) {
            putString("google_id_token", acct.idToken)
            apply()
        }
        mAuth.signInWithCredential(credential)
            .addOnSuccessListener(this, this::setUpAccountAndRedirectToHome)
            .addOnFailureListener(this, this::showFailureMessageAndLogException)
    }

    private fun handleFacebookAccessToken(token: AccessToken) {
        val cred = FacebookAuthProvider.getCredential(token.token)
        val sharedPref = this.getSharedPreferences("auth", Context.MODE_PRIVATE) ?: return
        with(sharedPref.edit()) {
            putString("facebook_id_token", token.token)
            apply()
        }
        mAuth.signInWithCredential(cred)
            .addOnSuccessListener(this, this::setUpAccountAndRedirectToHome)
            .addOnFailureListener(this, this::showFailureMessageAndLogException)
    }

    private fun setUpAccountAndRedirectToHome(authResult: AuthResult) {
        if (authResult.additionalUserInfo?.isNewUser == true) {
            hideButtonsAndShowLoader()
            dataRepository.setupUser(object : RepositoryCallback<Void> {
                override fun onSuccess(data: Void?) {
                    showButtonsAndHideLoader()
                    setResult(RESULT_OK)
                    finish()
                }

                override fun onError(t: Throwable) {
                    Timber.e(t)
                    showButtonsAndHideLoader()
                    setResult(RESULT_OK)
                    finish()
                }
            })
        } else {
            setResult(RESULT_OK)
            finish()
        }
    }

    private fun validateInput(): Boolean {
        return binding.etEmail.nonEmpty {
            binding.etEmail.error = getString(R.string.error_validation_empty_email)
        }
            && binding.etEmail.validEmail {
            binding.etEmail.error = getString(R.string.error_validation_invalid_email)
        }
            && binding.etPassword.nonEmpty {
            binding.etPassword.error = getString(R.string.error_validation_empty_password)
        }
    }

    private fun showFailureMessageAndLogException(e: Exception) {
        Timber.e(e)
        if (e is FirebaseAuthUserCollisionException) {
            Toast.makeText(this, getString(R.string.error_account_exist_with_email), Toast.LENGTH_LONG).show()
        } else {
            Toast.makeText(this, R.string.error_unknown, Toast.LENGTH_LONG).show()
        }
    }

    /**
     * hide action button (connect, register, forget) and show loader to prevent user action while
     * processing sign in action
     */
    private fun hideButtonsAndShowLoader() {
        binding.btnConnect.visibility = View.INVISIBLE
        binding.btnRegister.visibility = View.INVISIBLE
        binding.btnForget.visibility = View.INVISIBLE
        binding.btnFacebookSignin.visibility = View.INVISIBLE
        binding.btnGoogleSignin.visibility = View.INVISIBLE
        binding.pbLogin.visibility = View.VISIBLE
    }

    /**
     * Show action button (connect, register, forget) and hide loader
     */
    private fun showButtonsAndHideLoader() {
        binding.btnConnect.visibility = View.VISIBLE
        binding.btnRegister.visibility = View.VISIBLE
        binding.btnForget.visibility = View.VISIBLE
        binding.btnFacebookSignin.visibility = View.VISIBLE
        binding.btnGoogleSignin.visibility = View.VISIBLE
        binding.pbLogin.visibility = View.INVISIBLE
    }

    /**
     * Open a web page of a specified URL
     *
     * @param url URL to open
     */
    private fun openWebPage(url: String) {
        val uri = Uri.parse(url)
        val intent = Intent(Intent.ACTION_VIEW, uri)
        try {
            startActivity(intent)
        } catch (e: ActivityNotFoundException) {
            Toast.makeText(
                this,
                "No application can handle the link",
                Toast.LENGTH_SHORT
            ).show()
        }
    }

    override fun onBackPressed() {
        onBackPressedDispatcher.onBackPressed()
        setResult(RESULT_CANCELED)
        finish()
    }
}
