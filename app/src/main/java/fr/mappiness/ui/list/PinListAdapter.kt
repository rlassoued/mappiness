package fr.mappiness.ui.list

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.squareup.picasso.Picasso
import fr.mappiness.R
import fr.mappiness.data.Pin
import fr.mappiness.databinding.FragmentListPinsItemBinding
import kotlinx.android.extensions.LayoutContainer
class PinListAdapter(private val listener: (Pin) -> Unit) :
    ListAdapter<Pin, PinListAdapter.PinViewHolder>(DIFF_CALLBACK) {

    companion object {
        val DIFF_CALLBACK = object : DiffUtil.ItemCallback<Pin>() {
            override fun areItemsTheSame(oldItem: Pin, newItem: Pin): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: Pin, newItem: Pin): Boolean {
                return oldItem == newItem
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PinViewHolder {
        val v = LayoutInflater.from(parent.context)
            .inflate(R.layout.fragment_list_pins_item, parent, false)
        return PinViewHolder(v)
    }

    override fun onBindViewHolder(holder: PinViewHolder, position: Int) {
        holder.bind(getItem(position), listener)
    }

    class PinViewHolder(override val containerView: View) :
        androidx.recyclerview.widget.RecyclerView.ViewHolder(containerView), LayoutContainer {
        val binding = FragmentListPinsItemBinding.bind(containerView)
        fun bind(pin: Pin?, listener: (Pin) -> Unit) = with(containerView) {
            binding.tvDistance.visibility = View.INVISIBLE
            binding.ivEmoji.visibility = View.INVISIBLE
            binding.tvTag1.visibility = View.INVISIBLE
            binding.tvTag2.visibility = View.INVISIBLE
            pin?.let {
                setOnClickListener { listener(pin) }
                binding.tvTitle.text = pin.name
                binding.tvDescription.text = pin.description
                if (pin.distance >= 0) {
                    binding.tvDistance.text = context.getString(R.string.distance_format, pin.distance)
                    binding.tvDistance.visibility = View.VISIBLE
                }
                if (pin.tags.isNotEmpty()) {
                    binding.tvTag1.text = pin.tags[0].name
                    binding.tvTag1.visibility = View.VISIBLE
                }
                if (pin.geobooks.isNotEmpty()) {
                    binding.tvTag2.visibility = View.VISIBLE
                    binding.tvTag2.text = pin.geobooks[0].title
                    val drawableRes = when {
                        pin.geobooks[0].isFreeOfCharge -> R.drawable.tv_tag_green
                        else -> R.drawable.tv_tag_blue
                    }
                    binding.tvTag2.setBackgroundResource(drawableRes)
                } else if (pin.tags.size > 1) {
                    binding.tvTag2.text = pin.tags[1].name
                    binding.tvTag2.setBackgroundResource(R.drawable.tv_tag)
                    binding.tvTag2.visibility = View.VISIBLE
                }
                if (pin.emoticon != null) {
                    binding.ivEmoji.setImageResource(pin.emoticon.icon)
                    binding.ivEmoji.visibility = View.VISIBLE
                }
                if (pin.thumbnail != null) {
                    Picasso.Builder(context).build()
                        .load(pin.thumbnail)
                        .placeholder(R.drawable.image_placeholder)
                        .fit()
                        .centerCrop()
                        .error(R.drawable.image_placeholder)
                        .into(binding.imageView)
                } else {
                    Picasso.Builder(context).build()
                        .load(R.drawable.image_placeholder)
                        .placeholder(R.drawable.image_placeholder)
                        .error(R.drawable.image_placeholder)
                        .fit()
                        .centerCrop()
                        .into(binding.imageView)
                }
            }
        }

    }
}