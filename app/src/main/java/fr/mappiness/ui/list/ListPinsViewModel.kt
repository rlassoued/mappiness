package fr.mappiness.ui.list

import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import fr.mappiness.data.*
import timber.log.Timber

class ListPinsViewModel(private val dataRepository: DataRepository): ViewModel() {

    private val currentLocation = MutableLiveData<LatLng>()
    private val queryString = MutableLiveData<String>()
    private val page = MutableLiveData<Int>()
    val listing: MediatorLiveData<List<Pin>> = MediatorLiveData()

    init {
        listing.addSource(currentLocation) {
            Timber.d("currentLocation changed")
            dataRepository.listPins(
                    query = null,
                    lat = it?.lat,
                    lng = it?.lng,
                    distance = null,
                    page = 1,
                    perPage = PAGE_SIZE,
                    callback = object: RepositoryCallback<List<Pin>> {
                        override fun onSuccess(data: List<Pin>?) {
                            listing.postValue(data)
//                            if (data == null) {
//                                _pins.value = Ressource.error("empty response")
//                            } else {
//                                _pins.value = Ressource.success(data)
//                            }
                        }
                        override fun onError(t: Throwable) {
                            Timber.d(t)
//                            _pins.value = Ressource.error(t.localizedMessage)
                        }
                    }
            )
        }
        listing.addSource(queryString) {
            Timber.d("query string changed")
            dataRepository.listPins(
                query = it,
                lat = if (it == null) currentLocation.value?.lat else null,
                lng = if (it == null) currentLocation.value?.lng else null,
                distance = null,
                page = 1,
                perPage = PAGE_SIZE,
                callback = object: RepositoryCallback<List<Pin>> {
                    override fun onSuccess(data: List<Pin>?) {
                        listing.postValue(data)
//                            if (data == null) {
//                                _pins.value = Ressource.error("empty response")
//                            } else {
//                                _pins.value = Ressource.success(data)
//                            }
                    }
                    override fun onError(t: Throwable) {
                        Timber.d(t)
//                            _pins.value = Ressource.error(t.localizedMessage)
                    }
                }
            )
        }
        listing.addSource(page) {
            Timber.d("currentLocation changed")
            it?.let { page ->
                dataRepository.listPins(
                        query = queryString.value,
                        lat = if (queryString.value == null) currentLocation.value?.lat else null,
                        lng = if (queryString.value == null) currentLocation.value?.lng else null,
                        distance = null,
                        page = page,
                        perPage = PAGE_SIZE,
                        callback = object : RepositoryCallback<List<Pin>> {
                            override fun onSuccess(data: List<Pin>?) {
                                val newList = mutableListOf<Pin>().apply {
                                    addAll(listing.value ?: emptyList())
                                    addAll(data ?: emptyList())
                                }
                                listing.postValue(newList)
                            }
                            override fun onError(t: Throwable) {
                                Timber.d(t)
                            }
                        }
                )
            }
        }
    }

    fun showPins(lat: Double?, lng: Double?) {
        Timber.d("ShowPins(): %s, %s", lat, lng)
        currentLocation.postValue(LatLng(lat, lng))
    }

    fun reload(lat: Double? = currentLocation.value?.lat, lng: Double? = currentLocation.value?.lng) {
        currentLocation.postValue(LatLng(lat, lng))
        listing.value = emptyList()
    }

    fun updateEmoticon(pinId: Long, emoticon: Emoticon?) {
        val newList = listing.value?.map { if (it.id == pinId) it.copy(emoticon = emoticon) else it }
        listing.value = newList
    }

    fun loadNextPage(page: Int) {
        this.page.value = page
    }

    /**
     * Filter the search by query keywords
     */
    fun searchPins(query: String) {
        Timber.d("Search()")
        queryString.postValue(query)
    }

    fun stopSearch() {
        queryString.postValue(null)
    }

    data class LatLng(val lat: Double?, val lng: Double?)

    companion object {
        const val PAGE_SIZE = 30
    }

}