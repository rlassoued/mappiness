package fr.mappiness.ui.list

import android.app.Activity.RESULT_OK
import android.content.Intent
import android.location.Location
import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import fr.mappiness.R
import fr.mappiness.data.Emoticon
import fr.mappiness.data.Pin
import fr.mappiness.databinding.FragmentListPinsBinding
import fr.mappiness.ui.LocationChangedListener
import fr.mappiness.ui.details.PinDetailActivity
import fr.mappiness.ui.filter.FilterDialogFragment
import fr.mappiness.ui.filter.Filterable
import fr.mappiness.ui.map.PinDetailsFragment.Companion.ARG_PIN
import fr.mappiness.ui.map.PinsMapViewModel
import fr.mappiness.utils.consume
import org.koin.androidx.viewmodel.ext.android.activityViewModel
import timber.log.Timber


/**
 * A simple [Fragment] subclass.
 * Use the [ListPinsFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class ListPinsFragment :
    Fragment(),
    SearchView.OnQueryTextListener,
    LocationChangedListener,
    Filterable {

    private val model: ListPinsViewModel by activityViewModel()
    private val mapModel: PinsMapViewModel by activityViewModel()

    private lateinit var binding: FragmentListPinsBinding

    private var mostRecentLocation: Location? = null
    private var firstLocationUpdate: Boolean = true

    private lateinit var scrollListener: EndlessRecyclerViewScrollListener

    private val REQUEST_CODE_KEY = "request_code"
    private var resultLauncher =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            result.data?.let { data ->
                if (result.resultCode == RESULT_OK &&
                    data.getIntExtra(REQUEST_CODE_KEY, -1) == REQUEST_EMOTICON_UPDATE
                ) {
                    Timber.d("Pin was updated")
                    val pinId = data.getLongExtra(PinDetailActivity.EXTRA_PIN_ID, 0)
                    val pinEmoticon = data.getSerializableExtra(PinDetailActivity.EXTRA_EMOTICON) as? Emoticon
                    mapModel.onUpdatePinEmoticon(pinId, pinEmoticon)
                    model.updateEmoticon(pinId, pinEmoticon)
                }
            }
        }
    /**
     * Create the adapter and pass the onClickListener that will be attach to each pins.
     */
    private val mAdapter = PinListAdapter { pin: Pin ->
        val intent = Intent(activity, PinDetailActivity::class.java)
        intent.putExtra(ARG_PIN, pin)
        intent.putExtra(REQUEST_CODE_KEY, REQUEST_EMOTICON_UPDATE)
        resultLauncher.launch(intent)
    }

    private var mSearchView: SearchView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        binding = FragmentListPinsBinding.inflate(layoutInflater,container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val layoutManager = androidx.recyclerview.widget.LinearLayoutManager(this@ListPinsFragment.activity)

        binding.listPinFragmentRecyclerView.apply {
            this.setHasFixedSize(true)
            this.adapter = mAdapter
            this.layoutManager = layoutManager
        }

        scrollListener = object : EndlessRecyclerViewScrollListener(layoutManager) {
            override fun onLoadMore(page: Int, totalItemsCount: Int, view: androidx.recyclerview.widget.RecyclerView) {
                model.loadNextPage(page)
            }
        }

        binding.listPinFragmentRecyclerView.addOnScrollListener(scrollListener)

        binding.listPinFragmentProgressbar.visibility = View.GONE

        model.listing.observe(viewLifecycleOwner, Observer {
            mAdapter.submitList(it)
        })
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.map_and_list_toolbar_actions, menu)
        val searchItem = menu.findItem(R.id.action_search)
        mSearchView = searchItem?.actionView as SearchView
        mSearchView?.setOnQueryTextListener(this)
        searchItem.setOnActionExpandListener(object : MenuItem.OnActionExpandListener {
            override fun onMenuItemActionExpand(item: MenuItem): Boolean {
                return true
            }

            override fun onMenuItemActionCollapse(item: MenuItem): Boolean {
                scrollListener.resetState()
                model.stopSearch()
                return true
            }
        })

        val filterView = menu.findItem(R.id.action_filter).actionView
        val filterBtn = filterView?.findViewById<ImageButton>(R.id.imageButton)

        filterBtn?.setOnClickListener {
            showFilterDialog()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        R.id.action_filter -> consume { showFilterDialog() }
        else -> super.onOptionsItemSelected(item)
    }

    /**
     * Implementation of LocationChangedListener.
     * It trigger the loading of the pin once the location has been successfully acquired.
     */
    override fun onLocationChange(location: Location) {
        if (firstLocationUpdate) {
            firstLocationUpdate = false
            scrollListener.resetState()
            model.showPins(location.latitude, location.longitude)
        }
        mostRecentLocation = location
    }

    /**
     * Implementation of LocationChangedListener.
     * It trigger the loading of the pin without location.
     */
    override fun onNoLocation() {
        scrollListener.resetState()
        model.showPins(null, null)
    }

    override fun onQueryTextSubmit(query: String) = consume {
        scrollListener.resetState()
        model.searchPins(query)
        mSearchView?.clearFocus()
    }

    override fun onQueryTextChange(query: String) = consume { }

    override fun onSelectedFilterSave() {
        scrollListener.resetState()
        model.reload()
        mapModel.onPrefChanged()
    }

    private fun showFilterDialog() {
        fragmentManager?.let {
            FilterDialogFragment.newInstance().apply {
                setTargetFragment(this@ListPinsFragment, 0)
                show(it, "fragment_edit_name")
            }
        }
    }

    companion object {
        const val REQUEST_EMOTICON_UPDATE = 2

        const val TAG = "list"

        fun newInstance(): ListPinsFragment {
            return ListPinsFragment()
        }
    }

}
