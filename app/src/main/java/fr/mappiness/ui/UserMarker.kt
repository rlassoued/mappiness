package fr.mappiness.ui

import android.content.Context
import android.graphics.*
import com.google.android.gms.maps.model.BitmapDescriptor
import fr.mappiness.R
import java.io.File

class UserMarker {
    companion object {

        fun getMarker(context: Context): Bitmap {
            val file = File(context.cacheDir, "profile-picture.tmp")
            var profilePicture = if(file.exists()) BitmapFactory.decodeStream(file.inputStream()) else null
            return if (profilePicture != null) {
                getPersonalizedMarker(context, profilePicture)
            } else {
                profilePicture = BitmapFactory.decodeResource(context.resources, R.drawable.image_placeholder)
                getPersonalizedMarker(context, profilePicture)
            }
        }

        private fun getPersonalizedMarker(context: Context, profilePicture: Bitmap): Bitmap {
            val markerShape = BitmapFactory.decodeResource(context.resources, R.drawable.user_marker_shape)
            val borderWidth = 20
            val pictureSize = markerShape.width - borderWidth
            val profilePictureScaled = getCroppedBitmap(Bitmap.createScaledBitmap(profilePicture, pictureSize, pictureSize, false))
            val conf = Bitmap.Config.ARGB_8888
            val output = Bitmap.createBitmap(markerShape.width, markerShape.height, conf)
            val canvas = Canvas(output)
            val color = Paint()
            color.textSize = 35f
            color.color = Color.BLACK
            canvas.drawBitmap(markerShape, 0f, 0f, color)
            canvas.drawBitmap(profilePictureScaled, borderWidth / 2f, borderWidth / 2f, color)
            canvas.save()
            return output
        }

        fun getCroppedBitmap(bitmap: Bitmap): Bitmap {
            val output = Bitmap.createBitmap(bitmap.width, bitmap.height, Bitmap.Config.ARGB_8888)
            val canvas = Canvas(output)

            val color = 0xFF424242.toInt()
            val paint =  Paint()
            val rect = Rect(0, 0, bitmap.width, bitmap.height)

            paint.isAntiAlias = true
            canvas.drawARGB(0, 0, 0, 0)
            paint.color = color
            canvas.drawCircle(bitmap.width / 2f, bitmap.height / 2f,
                    bitmap.width / 2f, paint)
            paint.xfermode = PorterDuffXfermode(PorterDuff.Mode.SRC_IN)
            canvas.drawBitmap(bitmap, rect, rect, paint)
            return output
        }
    }
}