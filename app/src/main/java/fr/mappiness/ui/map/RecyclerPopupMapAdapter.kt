package fr.mappiness.ui.map

import android.location.Location
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.DiffUtil
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.gms.maps.model.LatLng
import com.squareup.picasso.Picasso
import fr.mappiness.R
import fr.mappiness.data.Pin
import fr.mappiness.databinding.ItemPopupMapBinding
import kotlinx.android.extensions.LayoutContainer
/**
 * Created by tran on 15/06/2017.
 */

class RecyclerPopupMapAdapter(private val listener: (Pin) -> Unit) :
    ListAdapter<Pin, RecyclerPopupMapAdapter.Holder>(DIFF_CALLBACK)
{
    var currentLocation : LatLng? = null
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_popup_map, parent, false)
        return Holder(view)
    }

    class Holder(override val containerView: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(containerView), LayoutContainer {
         val binding = ItemPopupMapBinding.bind(containerView)
        fun bind(pin: Pin?, listener: (Pin) -> Unit, currentLocation: LatLng?) = with(containerView) {
            binding.tvDistance.visibility = View.INVISIBLE
            binding.ivEmoji.visibility = View.INVISIBLE

            pin?.let {
                binding.tvTitle.text = it.name
                binding.tvDescription.text = it.description
                setOnClickListener { _ -> listener(pin) }
                if (pin.distance >= 0 && currentLocation != null) {
                    val dist = Location("current").apply {
                        longitude = currentLocation.longitude
                        latitude = currentLocation.latitude
                    }.distanceTo(Location("pin").apply {
                        latitude = pin.latitude
                        longitude = pin.longitude
                    }).div(1000)
                    binding.tvDistance.text = context.getString(R.string.distance_format, dist)
                    binding.tvDistance.visibility = View.VISIBLE
                }
                if (it.emoticon != null) {
                    binding.ivEmoji.setImageResource(it.emoticon.icon)
                    binding.ivEmoji.visibility = View.VISIBLE
                }
                if (it.thumbnail != null) {
                    Picasso.Builder(context).build()
                        .load(it.thumbnail)
                        .placeholder(R.drawable.image_placeholder)
                        .fit()
                        .centerCrop()
                        .error(R.drawable.image_placeholder)
                        .into(binding.ivPhoto)
                } else {
                    Picasso.Builder(context).build()
                        .load(R.drawable.image_placeholder)
                        .placeholder(R.drawable.image_placeholder)
                        .error(R.drawable.image_placeholder)
                        .fit()
                        .centerCrop()
                        .into(binding.ivPhoto)
                }
            }
        }

    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        holder.bind(getItem(position), listener,currentLocation)
    }

    companion object {
        val DIFF_CALLBACK = object : DiffUtil.ItemCallback<Pin>() {
            override fun areItemsTheSame(oldItem: Pin, newItem: Pin): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: Pin, newItem: Pin): Boolean {
                return oldItem == newItem
            }
        }
    }

}
