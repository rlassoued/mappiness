package fr.mappiness.ui.map


//import com.google.android.gms.location.places.Places
import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.widget.ImageView
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.google.android.material.chip.Chip
import com.squareup.picasso.Picasso
import fr.mappiness.R
import fr.mappiness.data.Emoticon
import fr.mappiness.data.Pin
import fr.mappiness.databinding.FragmentPinInfoBinding
import fr.mappiness.ui.AuthorProfile
import fr.mappiness.ui.HomeActivity
import fr.mappiness.ui.SimplePhotoActivity
import fr.mappiness.ui.carouselview.ImageClickListener
import fr.mappiness.ui.carouselview.ImageListener
import fr.mappiness.ui.geobooks.GeobookDetailActivity
import fr.mappiness.ui.list.ListPinsViewModel
import fr.mappiness.utils.consume
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber

/**
 * A simple [Fragment] subclass.
 */
class PinDetailsFragment : BottomSheetDialogFragment(), ImageListener, ImageClickListener {

    private val mapFragmentModel: PinsMapViewModel by activityViewModels()
    private val listFragmentModel: ListPinsViewModel by activityViewModels()
    private val model: PinDetailsViewModel by viewModel()

    private lateinit var pin: Pin
    private lateinit var binding: FragmentPinInfoBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        pin = requireArguments().getParcelable(ARG_PIN)!!
        binding = FragmentPinInfoBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.myToolbar.inflateMenu(R.menu.pin_info_toolbar_actions)
        binding.myToolbar.setNavigationIcon(R.drawable.ic_close_black_24dp)
        binding.myToolbar.setNavigationOnClickListener { dismiss() }
        binding.myToolbar.setOnMenuItemClickListener(::onMenuItemClick)
        model.fetchPlaceDetail(pin.place_id)

        model.emoticon.observe(viewLifecycleOwner, Observer {
            updateDisplayedEmoticon(it)
            mapFragmentModel.onUpdatePinEmoticon(pin.id, it)
            listFragmentModel.updateEmoticon(pin.id, it)
        })
        binding.showListGeobook.setOnClickListener {
            val intent = Intent(activity, GeobookDetailActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_SINGLE_TOP
            intent.putExtra(GeobookDetailActivity.ARG_GEOBOOK, pin.geobooks.first { !it.isFreeOfCharge and !it.isPurchased })
            intent.putExtra(ARG_PIN,pin)
            activity?.startActivityForResult(intent, HomeActivity.REQUEST_PURCHASE_GEOBOOK)
        }
        updateUI(pin)
    }

    private fun onMenuItemClick(item: MenuItem?) = when (item?.itemId) {
        R.id.action_heart -> consume { model.updateEmoticon(pin.id, Emoticon.HEART) }
        R.id.action_thumbs_up -> consume { model.updateEmoticon(pin.id, Emoticon.THUMBS_UP) }
        R.id.action_seen -> consume { model.updateEmoticon(pin.id, Emoticon.CHECKED) }
        R.id.action_to_see -> consume { model.updateEmoticon(pin.id, Emoticon.TO_SEE) }
        R.id.action_thumbs_down -> consume { model.updateEmoticon(pin.id, Emoticon.THUMBS_DOWN) }
        R.id.action_remove -> consume { model.removeEmoticon(pin.id) }
        else -> false
    }

    private fun updateDisplayedEmoticon(emoticon: Emoticon?) {
        val emoticonIcon = emoticon?.icon ?: R.drawable.ic_insert_emoticon_black_24dp
        binding.myToolbar.menu.findItem(R.id.action_emoji)?.setIcon(emoticonIcon)
    }

    private fun updateUI(pin: Pin) {
        binding.myToolbar.title = pin.address
        if (pin.geobooks.isNotEmpty() and pin.geobooks.all { !it.isFreeOfCharge } and pin.geobooks.all { !it.isPurchased }) {
            binding.tvPinDescription.setLines(5)
        }
        binding.tvPinDescription.text = pin.description
        if (pin.geobooks.isNotEmpty() and pin.geobooks.all { !it.isFreeOfCharge } and pin.geobooks.all { !it.isPurchased }) {
            binding.showListGeobook.visibility = VISIBLE
        } else {
            binding.showListGeobook.visibility = GONE
        }
        binding.tvWebsite.visibility = GONE
        binding.tvPhone.visibility = GONE
        binding.tvOpeningHours.visibility = GONE
        model.placeDetail.observe(viewLifecycleOwner, Observer {
            Timber.d("place detail: %s", it)
            it?.result?.website?.let { url ->
                binding.tvWebsite.text = url
                binding.tvWebsite.visibility = VISIBLE
            }
            it?.result?.formatted_phone_number?.let { phoneNumber ->
                binding.tvPhone.text = phoneNumber
                binding.tvPhone.visibility = VISIBLE
            }
            it?.result?.opening_hours?.let { openingHours ->
                binding.tvOpeningHours.text = openingHours.open_now?.let { openNow -> if (openNow) "${getString(R.string.ouvert)}" else "${getString(R.string.fermer)}" }
                    ?: ""
                binding.tvOpeningHours.visibility = VISIBLE
                openingHours.weekday_text?.let { weekday_text ->
                    binding.tvOpeningHoursDetails.text = weekday_text.joinToString(separator = "\n")
                    binding.tvOpeningHours.setOnClickListener {
                        binding.expandableLayout.toggle()
                    }
                }
            }
        })
        binding.tvPinName.text = pin.name
        binding.tvAddr.text = pin.address
        pin.emoticon?.let { emoticon ->
            binding.myToolbar.menu?.findItem(R.id.action_emoji)?.setIcon(emoticon.icon)
        }

        binding.pinImage.setImageListener(this)
        binding.pinImage.setImageClickListener(this)
        binding.pinImage.pageCount = pin.images.count()

        binding.chipGrpTag.removeAllViews()
        pin.tags.forEach { tag ->
            binding.chipGrpTag.addView(Chip(binding.chipGrpTag.context).apply {
                text = tag.name
                ellipsize = TextUtils.TruncateAt.END
                isClickable = false
            })
        }
        pin.geobooks.forEach { geobook ->
            binding.chipGrpTag.addView(Chip(binding.chipGrpTag.context).apply {
                text = geobook.title
                isClickable = false
                ellipsize = TextUtils.TruncateAt.END
                val color = when {
                    geobook.isFreeOfCharge -> R.color.color_green
                    else -> R.color.color_blue
                }
                setChipBackgroundColorResource(color)
            })
        }
        binding.tvAuthorName.text = pin.author.name
        Picasso.Builder(requireContext()).build()
            .load(pin.author.picture)
            .placeholder(R.drawable.image_placeholder)
            .error(R.drawable.image_placeholder)
            .fit()
            .centerCrop()
            .into(binding.ivAuthor)
        binding.lytAuthor.setOnClickListener {
            val intent = Intent(activity, AuthorProfile::class.java)
            intent.putExtra(AuthorProfile.KEY_AUTHOR, pin.author)
            startActivity(intent)
        }
        binding.ivPartenaire.visibility = GONE
        if (pin.geobooks.isNotEmpty()) {
            pin.geobooks.forEach { System.out.println("exxx ${it.title} / ${it.isFreeOfCharge}-${it.isPurchased}-${it.partnerLogo}") }
            pin.geobooks.firstOrNull { it.isPurchased }?.partnerLogo?.let {
                binding.ivPartenaire.visibility = VISIBLE
                Picasso.Builder(requireContext()).build()
                    .load(it)
                    .placeholder(R.drawable.image_placeholder)
                    .fit()
                    .centerCrop()
                    .error(R.drawable.image_placeholder)
                    .into(binding.ivPartenaire as ImageView)
            }
        }
        Timber.d("Place_id: %s", pin.place_id)
    }

    override fun onClick(position: Int) {
        val intent = Intent(activity, SimplePhotoActivity::class.java)
        intent.putExtra(SimplePhotoActivity.ARG_PIN_IMAGE, pin.images[position])
        startActivity(intent)
    }

    override fun setImageForPosition(position: Int, imageView: ImageView?) {
        Picasso.Builder(requireContext()).build()
            .load(pin.images[position])
            .placeholder(R.drawable.image_placeholder)
            .error(R.drawable.image_placeholder)
            .fit()
            .centerCrop()
            .into(imageView)
    }

    companion object {

        const val ARG_PIN = "arg_pin"

        fun newInstance(pin: Pin): PinDetailsFragment {
            val args = Bundle()
            args.putParcelable(ARG_PIN, pin)
            val fragment = PinDetailsFragment()
            fragment.arguments = args
            return fragment
        }
    }

}
