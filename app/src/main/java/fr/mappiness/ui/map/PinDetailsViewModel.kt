package fr.mappiness.ui.map

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
//import com.google.android.gms.location.places.Place
import fr.mappiness.GooglePlace.GooglePlace
import fr.mappiness.data.DataRepository
import fr.mappiness.data.Emoticon
import fr.mappiness.data.PlaceDetail
import fr.mappiness.data.RepositoryCallback
import timber.log.Timber

class PinDetailsViewModel(
        private val dataRepository: DataRepository,
        private val googlePlace: GooglePlace
): ViewModel() {

    val emoticon: LiveData<Emoticon>
        get() = _emoticon

    val placeDetail: LiveData<PlaceDetail>
        get() = _pinDetail

    private val _emoticon = MutableLiveData<Emoticon>()
    private val _pinDetail = MutableLiveData<PlaceDetail>()

    fun updateEmoticon(pinID: Long, emoticon: Emoticon) {
        dataRepository.updatePinEmoticon(pinID, emoticon, object : RepositoryCallback<Void> {
            override fun onSuccess(data: Void?) {
                Timber.d("Update Emoticon")
                _emoticon.value = emoticon
            }
            override fun onError(t: Throwable) {}
        })
    }

    fun removeEmoticon(pinId: Long) {
        dataRepository.removePinEmoticon(pinId, object: RepositoryCallback<Void>{
            override fun onSuccess(data: Void?) {
                _emoticon.value = null
            }
            override fun onError(t: Throwable) {}
        })
    }

    fun fetchPlaceDetail(placeId: String) {
        googlePlace.detail(placeId, object: RepositoryCallback<PlaceDetail>{
            override fun onSuccess(data: PlaceDetail?) {
                _pinDetail.postValue(data)
            }
            override fun onError(t: Throwable) {
                Timber.e(t)
            }
        })
    }

}