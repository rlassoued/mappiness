package fr.mappiness.ui.map

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import fr.mappiness.data.DataRepository
import fr.mappiness.data.Emoticon
import fr.mappiness.data.Pin
import fr.mappiness.data.RepositoryCallback
import fr.mappiness.data.Ressource
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.async
import kotlinx.coroutines.launch

class PinsMapViewModel(private val dataRepository: DataRepository) : ViewModel() {

    /**
     * This is the job for all coroutines started by this ViewModel.
     * Cancelling this job will cancel all coroutines started by this ViewModel.
     */
    private val viewModelJob = SupervisorJob()

    /**
     * This is the main scope for all coroutines launched by MainViewModel.
     * Since we pass viewModelJob, you can cancel all coroutines
     * launched by uiScope by calling viewModelJob.cancel()
     */
     val uiScope = CoroutineScope(Dispatchers.Main + viewModelJob)

    /**
     * Cancel all coroutines when the ViewModel is cleared
     */
    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }

    val pins: LiveData<Ressource<List<Pin>>>
        get() = _pins
    private val _pins = MutableLiveData<Ressource<List<Pin>>>()

    val prefChanged: LiveData<Boolean>
        get() = _prefChanged
    private val _prefChanged = MutableLiveData<Boolean>()

    fun init(
        query: String? = null,
        lat: Double? = null,
        lng: Double? = null,
        distance: Double = 1.0,
        page: Int = DEFAULT_PAGE,
        perPage: Int = DEFAULT_PER_PAGE
    ) {
        dataRepository.listPins(
            query = query,
            lat = lat,
            lng = lng,
            distance = distance,
            page = page,
            perPage = perPage,
            callback = object: RepositoryCallback<List<Pin>> {
                override fun onSuccess(data: List<Pin>?) {
                    if (data == null) {
                        _pins.value = Ressource.error("empty response")
                    } else {
                        _pins.value = Ressource.success(data)
                    }
                }
                override fun onError(t: Throwable) {
                    _pins.value = Ressource.error(t.localizedMessage)
                }
            }
        )
    }

    fun onUpdatePinEmoticon(pinID: Long, emoticon: Emoticon?) {
        _pins.value?.data?.let { pins ->
            val updatedList = pins.map {
                if (it.id == pinID) it.copy(emoticon = emoticon) else it
            }
            _pins.value = Ressource.success(updatedList)
        }
    }

    fun onPrefChanged() {
        _prefChanged.postValue(true)
    }

    suspend fun getPinsByGeobookId(id: Long) = dataRepository.listPinsByGeobookID(id)


    companion object {
        const val DEFAULT_PAGE = 1
        const val DEFAULT_PER_PAGE = 60
    }

}