package fr.mappiness.ui.map

import android.content.Intent
import android.graphics.Color
import android.location.Location
import android.os.Bundle
import android.os.SystemClock
import android.view.*
import android.view.View.GONE
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.Toast
import androidx.appcompat.widget.SearchView
import androidx.lifecycle.Observer
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.*
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.firebase.auth.FirebaseAuth
import fr.mappiness.R
import fr.mappiness.SharedPrefHelper
import fr.mappiness.data.Pin
import fr.mappiness.data.Ressource
import fr.mappiness.databinding.FragmentMapBinding
import fr.mappiness.ui.HomeActivity
import fr.mappiness.ui.LocationChangedListener
import fr.mappiness.ui.UserMarker
import fr.mappiness.ui.details.PinDetailActivity
import fr.mappiness.ui.filter.FilterDialogFragment
import fr.mappiness.ui.filter.Filterable
import fr.mappiness.ui.geobooks.GeobookDetailActivity
import fr.mappiness.ui.list.ListPinsViewModel
import fr.mappiness.ui.map.PinDetailsFragment.Companion.ARG_PIN
import fr.mappiness.utils.consume
import fr.mappiness.utils.extends
import fr.mappiness.utils.radius
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.activityViewModel
import timber.log.Timber
import uk.co.deanwild.materialshowcaseview.MaterialShowcaseSequence
import uk.co.deanwild.materialshowcaseview.ShowcaseConfig
class MapFragment : androidx.fragment.app.Fragment(),
    Filterable,
    OnMapReadyCallback,
    LocationChangedListener,
    SearchView.OnQueryTextListener {

    private val model: PinsMapViewModel by activityViewModel()
    private val listModel: ListPinsViewModel by activityViewModel()
    private val auth by lazy { FirebaseAuth.getInstance() }
    private lateinit var binding: FragmentMapBinding

    private var searchView: SearchView? = null

    private var googleMap: GoogleMap? = null
    private var currentLocationMarker: Marker? = null
    private var firstLocationUpdate = true

    private val adapter = RecyclerPopupMapAdapter {
        val bottomSheetDialogFragment = PinDetailsFragment.newInstance(it)
        bottomSheetDialogFragment.show(
            childFragmentManager,
            bottomSheetDialogFragment.tag
        )
    }

    private val googleMapGlobalLayoutListener = ViewTreeObserver.OnGlobalLayoutListener {
        googleMap?.setPadding(0, 0, 0, binding.rvPopup.height)
    }

    private val mapAdapter by lazy { MapAdapter(requireContext(), googleMap!!, binding.rvPopup) }

    override fun onCreate(savedInstanceState: Bundle?) {
        Timber.d("onCreate")
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        Timber.d("onCreateView")
        binding = FragmentMapBinding.inflate(layoutInflater,container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        Timber.d("onViewCreated")
        super.onViewCreated(view, savedInstanceState)
        binding.rvPopup.apply {
            adapter = this@MapFragment.adapter
            layoutManager =
                androidx.recyclerview.widget.LinearLayoutManager(this@MapFragment.context).apply {
                    orientation = androidx.recyclerview.widget.LinearLayoutManager.HORIZONTAL
                }
        }
        val snapHelper = androidx.recyclerview.widget.LinearSnapHelper()
        snapHelper.attachToRecyclerView(binding.rvPopup)

        binding.btnReload.visibility = View.GONE
        binding.btnReload.setOnClickListener {
            loadPins(null)
            binding.btnReload.visibility = View.GONE
        }

        binding.btnMyLocation.setOnClickListener {
            currentLocationMarker?.position?.let {
                googleMap?.animateCamera(CameraUpdateFactory.newLatLng(it))
            }
        }

        binding.map.onCreate(arguments)
        binding.map.getMapAsync(this)

    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.map_and_list_toolbar_actions, menu)
        val searchItem = menu.findItem(R.id.action_search)
        searchView = searchItem?.actionView as SearchView
        searchView?.setOnQueryTextListener(this)
        searchItem.setOnActionExpandListener(object : MenuItem.OnActionExpandListener {
            override fun onMenuItemActionExpand(item: MenuItem) = consume {}

            override fun onMenuItemActionCollapse(item: MenuItem) = consume {
                loadPins(null)
            }
        })

        val switchView = menu.findItem(R.id.enable_location_notification).actionView
        val filterView = menu.findItem(R.id.action_filter).actionView
        val filterBtn = filterView?.findViewById<ImageButton>(R.id.imageButton)

        filterBtn?.setOnClickListener {
            showFilterDialog()
        }

        if (SharedPrefHelper.getShouldShowShowCase(requireContext())) {
            val config = ShowcaseConfig()
            config.maskColor = Color.parseColor("#d9000000")
            val view = (requireActivity() as HomeActivity).binding.showcase
            val sequence = MaterialShowcaseSequence(activity).apply {
                setConfig(config)
                searchItem.actionView
                addSequenceItem(
                    switchView,
                    getString(R.string.tuto_notifications_title),
                    getString(R.string.tuto_notification_text),
                    getString(R.string.tuto_action_continue)
                )
                addSequenceItem(
                    filterView,
                    getString(R.string.tuto_filter_title),
                    getString(R.string.tuto_filter_text),
                    getString(R.string.tuto_action_continue)
                )
                addSequenceItem(
                    view,
                    getString(R.string.tuto_geobook_title),
                    getString(R.string.tuto_geobook_text),
                    getString(R.string.tuto_action_end)
                )
            }
            sequence.setOnItemDismissedListener { itemView, position ->
                SharedPrefHelper.setShouldShowShowCase(requireContext(), false)
                (requireActivity() as HomeActivity).binding.showcase.visibility = GONE
            }
            sequence.start()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        R.id.action_filter -> consume { showFilterDialog() }
        else -> super.onOptionsItemSelected(item)
    }

    override fun onQueryTextSubmit(query: String) = consume {
        loadPins(query)
        searchView?.clearFocus()
    }

    override fun onQueryTextChange(query: String) = consume {}

    /**
     * onMapReady is called asynchronously when the map is ready to be used.
     */
    override fun onMapReady(map: GoogleMap) {
        Timber.d("On map ready")
        googleMap = map
        binding.rvPopup.viewTreeObserver?.addOnGlobalLayoutListener(googleMapGlobalLayoutListener) // Set a bottom padding on the GoogleMap view for the logo to be visible
        googleMap?.setOnMarkerClickListener(::onMarkerClick)
    }

    /**
     * Implementation of LocationChangedListener. Trigger the loading of nearby pins
     */
    override fun onLocationChange(location: Location) {
//        Timber.d("LocationResult: %s", location)
        if (firstLocationUpdate) {
            Timber.d("First LocationResult !")
            googleMap?.moveCamera(
                CameraUpdateFactory.newLatLng(
                    LatLng(
                        location.latitude,
                        location.longitude
                    )
                )
            )
            notifyUILocationAvailability(location)
            firstLocationUpdate = false
        }
        currentLocationMarker?.position = LatLng(location.latitude, location.longitude)
        adapter.currentLocation = LatLng(location.latitude, location.longitude)
    }

    /**
     * Implementation of LocationChangedListener. Trigger the loading of pins without location
     */
    override fun onNoLocation() {
        showNoLocationUI()
    }

    private fun notifyUILocationAvailability(location: Location?) {
        Timber.d("Location is %s\n", location)
        loadPins(null)
        setDataChangeObservers()

        location?.let { it ->
            val currentPosition = MarkerOptions().apply {
                position(LatLng(it.latitude, it.longitude))
            }
            context?.let { context ->
                val bitmapMarker = UserMarker.getMarker(context)
                currentPosition.icon(BitmapDescriptorFactory.fromBitmap(bitmapMarker))
            }
            currentLocationMarker = googleMap?.addMarker(currentPosition)
        }

    }

   fun resetListPin(){
       updateUI(null)
       updateMap(null)
       loadPins(null,1.0)
       binding.btnReload.visibility = View.GONE

   }

    private fun showNoLocationUI() {
        Timber.d("Not location permission given")
        notifyUILocationAvailability(null)
    }

    override fun onStart() {
        super.onStart()
        binding.map.onStart()
        Timber.d("onStart")
    }

    override fun onResume() {
        super.onResume()
        binding.map.onResume()
        context?.let { context ->
            currentLocationMarker?.let { marker ->
                val bitmapMarker = UserMarker.getMarker(context)
                marker.setIcon(BitmapDescriptorFactory.fromBitmap(bitmapMarker))
            }
        }
        Timber.d("onResume")
    }

    override fun onPause() {
        super.onPause()
        binding.map.onPause()
        Timber.d("onPause")
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding.map.onDestroy()
        Timber.d("onDestroyView")
        binding.rvPopup.viewTreeObserver.removeOnGlobalLayoutListener(googleMapGlobalLayoutListener)
    }

    override fun onStop() {
        super.onStop()
        binding.map.onStop()
        Timber.d("onStop")
    }

    override fun onDestroy() {
        super.onDestroy()
        Timber.d("onDestroy")
    }

    override fun onLowMemory() {
        super.onLowMemory()
        binding.map.onLowMemory()
        Timber.d("onLowMemory")
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        binding.map.onSaveInstanceState(outState)
        Timber.d("onSaveInstanceState")
    }

    override fun onSelectedFilterSave() {
        loadPins(null)
        listModel.reload()
    }

    private fun showFilterDialog() {
        fragmentManager?.let {
            FilterDialogFragment.newInstance().apply {
                setTargetFragment(this@MapFragment, 0)
                show(it, "fragment_edit_name")
            }
        }
    }

    private fun onMarkerClick(marker: Marker): Boolean {
        if (marker.id == currentLocationMarker?.id) {
            return false
        }
        mapAdapter.submitCurrentPinIndex(marker.tag as Int)
        return true
    }

    private fun setDataChangeObservers() {
        model.prefChanged.observe(this, Observer {
            loadPins(null)
        })
        model.pins.observe(this, Observer {
            when (it?.status) {
                Ressource.Status.LOADING -> Timber.d("Loading")
                Ressource.Status.ERROR -> {
                    Toast.makeText(this.activity, R.string.error_unknown, Toast.LENGTH_LONG).show()
                }
                Ressource.Status.SUCCESS -> {
                    val geobookId = requireActivity().intent?.getLongExtra(GeobookDetailActivity.ARG_GEOBOOK,-1)!!
                    val lastSelectedPin = requireActivity().intent?.getParcelableExtra<Pin>(ARG_PIN)
                    model.uiScope.launch {
                        val datafiltred =
                            if (geobookId == -1L) it.data else model.getPinsByGeobookId(geobookId)
                        datafiltred?.let { _pins ->
                            updateUI(_pins)
                            updateMap(_pins)
                            if (geobookId != -1L) {
                                showAllMarkers(_pins)
                            }
                            // restore last pin details if is exist
                            if (lastSelectedPin != null) {
                                requireActivity().intent?.removeExtra(ARG_PIN)
                                view?.postDelayed({
                                    restoreLastScreen(
                                        _pins.indexOf(
                                            lastSelectedPin
                                        )
                                    )
                                }, 100)
                                val intent = Intent(requireActivity(), PinDetailActivity::class.java)
                                intent.putExtra(ARG_PIN, lastSelectedPin)
                                startActivity(intent)
                            }
                        }
                    }
                   Timber.d("Loaded %d pins", it.data?.size)
                }
                else -> {}
            }
        })

        googleMap?.setOnCameraIdleListener(object : DebouncedCameraIdleListener(1000) {
            override fun onDebouncedCameraIdle() {
                binding.btnReload.visibility = View.VISIBLE
            }
        })
    }

    private fun showAllMarkers(pins: List<Pin>) {
        val builder = LatLngBounds.Builder()
        for (m in pins) {
            builder.include(LatLng(m.latitude,m.longitude))
        }
        if(pins.isEmpty())
            return
        val bounds = builder.build()
        val width = resources.displayMetrics.widthPixels
        val height = resources.displayMetrics.heightPixels
        val padding = (width * 0.10).toInt()

        // Zoom and animate the google map to show all markers
        val cu = CameraUpdateFactory.newLatLngBounds(bounds, width, height, padding)
        googleMap?.animateCamera(cu)
    }
    private fun loadPins(query: String?, distance: Double? = null) = googleMap?.let { googleMap ->
        val bounds = googleMap.projection.visibleRegion.latLngBounds.extends(MARKER_BOUNDS_RATIO)
        model.init(
            query = query,
            lat = bounds.center.latitude,
            lng = bounds.center.longitude,
            distance = distance?:bounds.radius(),
            perPage = 150
        )
    }

    private fun updateUI(pins: List<Pin>?) {
        adapter.submitList(pins)
    }

    private fun updateMap(pins: List<Pin>?) {
        pins?.let { mapAdapter.submitList(it) }
    }

    private fun restoreLastScreen(position: Int) {
        binding.rvPopup.scrollToPosition(position)
    }

    companion object {
        const val TAG = "map"
        private const val MARKER_BOUNDS_RATIO = 4

        @JvmStatic
        fun newInstance(): MapFragment {
            return MapFragment()
        }
    }

}

abstract class DebouncedCameraIdleListener(private val minimumInterval: Long) :
    GoogleMap.OnCameraIdleListener {

    private var lastClickMap: Long? = null
    private var first = true

    abstract fun onDebouncedCameraIdle()

    override fun onCameraIdle() {
        if (first) {
            first = !first
            return
        }
        val previousClickTimestamp = lastClickMap
        val currentTimestamp = SystemClock.uptimeMillis()
        lastClickMap = currentTimestamp
        if (previousClickTimestamp == null || Math.abs(currentTimestamp - previousClickTimestamp) > minimumInterval) {
            onDebouncedCameraIdle()
        }
    }
}