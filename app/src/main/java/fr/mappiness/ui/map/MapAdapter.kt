package fr.mappiness.ui.map

import android.content.Context
import android.content.res.Resources
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import fr.mappiness.data.Pin
import kotlin.math.roundToInt

class MapAdapter(
    val context: Context,
    val googleMap: GoogleMap,
    val recyclerView: RecyclerView
) {

    private val _pins = mutableListOf<PinOnMap>()
    private var _currentPinIndex: Int = 0
    private var _prevPinIndex: Int? = null
    private val _onScrollListener = object : RecyclerView.OnScrollListener() {
        override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
            super.onScrollStateChanged(recyclerView, newState)
            val position = (recyclerView.layoutManager as LinearLayoutManager)
                .findFirstCompletelyVisibleItemPosition()
            if (position != RecyclerView.NO_POSITION) {
                submitCurrentPinIndex(position)
            }
        }
    }

    init {
        recyclerView.addOnScrollListener(_onScrollListener)
    }

    fun submitList(newPins: List<Pin>) {
        _pins.forEach { it.marker?.remove() }
        _pins.clear()
        _pins.addAll(newPins.map { PinOnMap(it) })
        _pins.mapIndexed { index, it ->
            it.marker = googleMap.addMarker(it.markerOptions)
            it.marker?.tag = index
            if (index == 0) {
                it.scaleUpMarkerIcon(context.resources)
            }
            it
        }
        _prevPinIndex = null
        _currentPinIndex = 0
    }

    fun submitCurrentPinIndex(index: Int) {
        _prevPinIndex = _currentPinIndex
        _currentPinIndex = index

        (recyclerView.layoutManager as LinearLayoutManager).scrollToPosition(_currentPinIndex)

        _pins[_currentPinIndex].marker?.let {
            val cameraUpdate = CameraUpdateFactory.newLatLng(it.position)
            googleMap.animateCamera(cameraUpdate)
        }

        _pins[_currentPinIndex].scaleUpMarkerIcon(context.resources)
        _pins[_prevPinIndex!!].scaleDownMarkerIcon()
    }

    fun getAtPosition(position: Int) = _pins[position]

    data class PinOnMap(val pin: Pin) {
        val markerOptions = MarkerOptions().apply {
            position(LatLng(pin.latitude, pin.longitude))
            icon(BitmapDescriptorFactory.fromResource(getDrawable(pin)))
        }
        var marker: Marker? = null

        fun scaleDownMarkerIcon() {
            marker?.setIcon(BitmapDescriptorFactory.fromResource(getDrawable(pin)))
            marker?.zIndex = 0f
        }

        private fun getDrawable(pin: Pin): Int {
            return when {
                pin.geobooks.any { it.isFreeOfCharge and it.isPurchased } -> pin.category.drawableIdGreen
                pin.geobooks.isNotEmpty() and pin.geobooks.all { !it.isFreeOfCharge } and pin.geobooks.any { it.isPurchased }-> pin.category.drawableIdBlue
                else -> pin.category.drawableIdYellow
            }
        }

        fun scaleUpMarkerIcon(resources: Resources) {
            val imageBitmap = BitmapFactory.decodeResource(resources, getDrawable(pin))
            val resizedBitmap = Bitmap.createScaledBitmap(
                imageBitmap,
                (imageBitmap.width * 1.5).roundToInt(),
                (imageBitmap.height * 1.5).roundToInt(),
                false
            )
            marker?.zIndex = 1.0f
            marker?.setIcon(BitmapDescriptorFactory.fromBitmap(resizedBitmap))
        }
    }

}