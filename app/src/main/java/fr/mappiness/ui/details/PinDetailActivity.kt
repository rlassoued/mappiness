package fr.mappiness.ui.details

import android.app.Activity
import android.content.Intent
import android.graphics.Color
import android.graphics.PorterDuff
import android.os.Bundle
import android.text.TextUtils
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.GoogleMapOptions
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.material.chip.Chip
import com.squareup.picasso.Picasso
import fr.mappiness.R
import fr.mappiness.data.Emoticon
import fr.mappiness.data.Pin
import fr.mappiness.databinding.ActivityPinDetailBinding
import fr.mappiness.ui.AuthorProfile
import fr.mappiness.ui.HomeActivity
import fr.mappiness.ui.SimplePhotoActivity
import fr.mappiness.ui.carouselview.ImageClickListener
import fr.mappiness.ui.carouselview.ImageListener
import fr.mappiness.ui.geobooks.GeobookDetailActivity
import fr.mappiness.ui.map.PinDetailsFragment.Companion.ARG_PIN
import fr.mappiness.utils.consume
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber


class PinDetailActivity : AppCompatActivity(), OnMapReadyCallback, ImageListener,
    ImageClickListener {


    private val model: PinDetailViewModel by viewModel()

    private var googleMap: GoogleMap? = null
    private var marker: Marker? = null
    private var menu: Menu? = null
    private lateinit var mPin: Pin

    private lateinit var binding : ActivityPinDetailBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityPinDetailBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setSupportActionBar(binding.toolbar)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        mPin = intent.getParcelableExtra(ARG_PIN)!!
        model.emoticon.observe(this, Observer {
            val emoticonIcon = it?.icon ?: R.drawable.ic_insert_emoticon_black_24dp
            val data = Intent().apply {
                this.putExtra(EXTRA_PIN_ID, mPin.id)
                this.putExtra(EXTRA_EMOTICON, it)
            }
            setResult(Activity.RESULT_OK, data)
            this.menu?.findItem(R.id.action_emoji)?.setIcon(emoticonIcon)
        })
        binding.contentIncluded.showListGeobookAct.setOnClickListener {
            val intent = Intent(this, GeobookDetailActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_SINGLE_TOP
            intent.putExtra(GeobookDetailActivity.ARG_GEOBOOK, mPin.geobooks.first { !it.isFreeOfCharge and !it.isPurchased })
            intent.putExtra(ARG_PIN,mPin)
            startActivityForResult(intent, HomeActivity.REQUEST_PURCHASE_GEOBOOK)
        }
        updateUI(mPin)
        initializeMap()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, intent: Intent?) = when (requestCode) {
        HomeActivity.REQUEST_PURCHASE_GEOBOOK -> onResultPurchasedGeobook(resultCode, intent)
        else -> super.onActivityResult(requestCode, resultCode, intent)
    }
    private fun onResultPurchasedGeobook(resultCode: Int, intent: Intent?) {
        if (resultCode == RESULT_OK) {
            val intente = Intent(this, HomeActivity::class.java)
            intente.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
            intent?.getParcelableExtra<Pin>(ARG_PIN)?.let { intente.putExtra(ARG_PIN, it) }
            intent?.getLongExtra(GeobookDetailActivity.ARG_GEOBOOK,-1)?.let { intente.putExtra(
                GeobookDetailActivity.ARG_GEOBOOK, it) }
            startActivity(intente)
            finish()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.pin_info_toolbar_actions, menu)
        menu?.findItem(R.id.action_emoji)?.icon?.setColorFilter(
            Color.WHITE,
            PorterDuff.Mode.SRC_ATOP
        )
        this.menu = menu
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        R.id.action_heart -> consume { model.addEmoticon(mPin.id,Emoticon.HEART) }
        R.id.action_thumbs_up -> consume { model.addEmoticon(mPin.id,Emoticon.THUMBS_UP) }
        R.id.action_seen -> consume { model.addEmoticon(mPin.id,Emoticon.CHECKED) }
        R.id.action_to_see -> consume { model.addEmoticon(mPin.id,Emoticon.TO_SEE) }
        R.id.action_thumbs_down -> consume { model.addEmoticon(mPin.id,Emoticon.THUMBS_DOWN) }
        R.id.action_remove -> consume { model.removeEmoticon(mPin.id) }
        else -> super.onOptionsItemSelected(item)
    }

    override fun onMapReady(map: GoogleMap) {
        googleMap = map
        model.pin.value?.let {
            updateUIMap(it.latitude, it.longitude)
        }
    }

    private fun updateUI(pin: Pin) {
        if (mPin.geobooks.isNotEmpty() and mPin.geobooks.all { !it.isFreeOfCharge } and mPin.geobooks.all { !it.isPurchased }) {
            binding.contentIncluded.tvPinDescription.setLines(5)
        }
        binding.contentIncluded.tvPinDescription.text = mPin.description
        if (mPin.geobooks.isNotEmpty() and mPin.geobooks.all { !it.isFreeOfCharge } and mPin.geobooks.all { !it.isPurchased }) {
            binding.contentIncluded.showListGeobookAct.visibility = View.VISIBLE
        } else {
            binding.contentIncluded.showListGeobookAct.visibility = View.GONE
        }
        binding.contentIncluded.tvWebsite.visibility = View.GONE
        binding.contentIncluded.tvPhone.visibility = View.GONE
        binding.contentIncluded.tvOpeningHours.visibility = View.GONE
        model.placeDetail.observe(this, Observer {
            Timber.d("Place detail: %s", it)
            it?.result?.website?.let { url ->
                binding.contentIncluded.tvWebsite.text = url
                binding.contentIncluded.tvWebsite.visibility = View.VISIBLE
            }
            it?.result?.formatted_phone_number?.let { phoneNumber ->
                binding.contentIncluded.tvPhone.text = phoneNumber
                binding.contentIncluded.tvPhone.visibility = View.VISIBLE
            }
            it?.result?.opening_hours?.let { openingHours ->
                binding.contentIncluded.tvOpeningHours.text =
                    openingHours.open_now?.let { openNow -> if (openNow) "Ouvert" else "Fermé" }
                        ?: ""
                binding.contentIncluded.tvOpeningHours.visibility = View.VISIBLE
                openingHours.weekday_text?.let { weekday_text ->
                    binding.contentIncluded.tvOpeningHoursDetails.text = weekday_text.joinToString(separator = "\n")
                    binding.contentIncluded.tvOpeningHours.setOnClickListener {
                        binding.contentIncluded.expandableLayout.toggle()
                    }
                }
            }
        })
        updateUIMap(pin.latitude, pin.longitude)
        binding.contentIncluded.tvPinName.text = pin.name
        binding.contentIncluded.tvAddr.text = pin.address
        binding.contentIncluded.mapAddr.text = pin.address
        pin.emoticon?.let { emoticon ->
            this.menu?.findItem(R.id.action_emoji)?.setIcon(emoticon.icon)
        }

        binding.pinImage.setImageListener(this)
        binding.pinImage.setImageClickListener(this)
        binding.pinImage.pageCount = pin.images.count()

        binding.contentIncluded.chipGrpTag.removeAllViews()
        pin.tags.forEach { tag ->
            binding.contentIncluded.chipGrpTag.addView(Chip(binding.contentIncluded.chipGrpTag.context).apply {
                text = tag.name
                ellipsize = TextUtils.TruncateAt.END
                isClickable = false
            })
        }
        pin.geobooks.forEach { geobook ->
            binding.contentIncluded.chipGrpTag.addView(Chip(binding.contentIncluded.chipGrpTag.context).apply {
                text = geobook.title
                isClickable = false
                ellipsize = TextUtils.TruncateAt.END
                val color = when {
                    geobook.isFreeOfCharge -> R.color.color_green
                    else -> R.color.color_blue
                }
                setChipBackgroundColorResource(color)
            })
        }
        binding.contentIncluded.lytAuthor.setOnClickListener {
            val intent = Intent(this, AuthorProfile::class.java)
            intent.putExtra(AuthorProfile.KEY_AUTHOR, pin.author)
            startActivity(intent)
        }
        binding.contentIncluded.ivPartenaire.visibility = View.GONE
        if (pin.geobooks.isNotEmpty()) {
            pin.geobooks.forEach { System.out.println("exxx ${it.title} / ${it.isFreeOfCharge}-${it.isPurchased}-${it.partnerLogo}") }
            pin.geobooks.firstOrNull { it.isPurchased }?.partnerLogo?.let {
                binding.contentIncluded.ivPartenaire.visibility = View.VISIBLE
                Picasso.Builder(this).build()
                    .load(it)
                    .placeholder(R.drawable.image_placeholder)
                    .fit()
                    .centerCrop()
                    .error(R.drawable.image_placeholder)
                    .into(binding.contentIncluded.ivPartenaire as ImageView)
            }
        }
        binding.contentIncluded.tvAuthorName.text = pin.author.name
        Picasso.Builder(this).build()
            .load(pin.author.picture)
            .placeholder(R.drawable.image_placeholder)
            .error(R.drawable.image_placeholder)
            .fit()
            .centerCrop()
            .into(binding.contentIncluded.ivAuthor)
    }

    private fun initializeMap() {
        val option = GoogleMapOptions()
            .liteMode(true)
        val mapFragment =
            supportFragmentManager.findFragmentById(R.id.map_frame_layout) as? SupportMapFragment
                ?: SupportMapFragment.newInstance(option)
        val ft = supportFragmentManager.beginTransaction()
        ft.replace(R.id.map_frame_layout, mapFragment)
        ft.commit()
        mapFragment.getMapAsync(this)
    }

    private fun updateUIMap(lat: Double, lng: Double) {
        val position = LatLng(lat, lng)
        marker?.let {
            it.position = position
        } ?: run {
            val options = MarkerOptions().apply {
                position(position)
            }
            marker = googleMap?.addMarker(options)
            googleMap?.animateCamera(CameraUpdateFactory.newLatLngZoom(position, 17f))
        }
    }

    override fun onClick(position: Int) {
        val intent = Intent(this, SimplePhotoActivity::class.java)
        intent.putExtra(SimplePhotoActivity.ARG_PIN_IMAGE, mPin?.images?.get(position))
        startActivity(intent)
    }

    override fun setImageForPosition(position: Int, imageView: ImageView?) {
        Picasso.Builder(this).build()
            .load(mPin?.images?.get(position))
            .placeholder(R.drawable.image_placeholder)
            .error(R.drawable.image_placeholder)
            .fit()
            .centerCrop()
            .into(imageView)
    }

    companion object {
        const val ARG_PIN_ID = "pin_id"
        const val EXTRA_PIN_ID = "pin_id"
        const val EXTRA_EMOTICON = "emoticon"
    }

}
