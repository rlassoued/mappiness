package fr.mappiness.ui.details

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import fr.mappiness.GooglePlace.GooglePlace
import fr.mappiness.data.*
import timber.log.Timber

class PinDetailViewModel(
        private val dataRepository: DataRepository,
        private val googlePlace: GooglePlace
): ViewModel() {

    private val _pin: MutableLiveData<Pin> = MutableLiveData()
    val pin: LiveData<Pin>
        get() = _pin

    private val _emoticon: MutableLiveData<Emoticon> = MutableLiveData()
    val emoticon: LiveData<Emoticon>
        get() = _emoticon

    private val _pinDetail = MutableLiveData<PlaceDetail>()
    val placeDetail: LiveData<PlaceDetail>
        get() = _pinDetail

    fun addEmoticon(pinID: Long, emoticon: Emoticon) {
            dataRepository.updatePinEmoticon(pinID, emoticon, object: RepositoryCallback<Void>{
                override fun onSuccess(data: Void?) {
                    Timber.d("Update Emoticon")
                    _emoticon.value = emoticon
                }
                override fun onError(t: Throwable) {
                    Timber.e(t)
                }
            })
    }

    /**
     * loadPin does the the request to load the given pin information and set the result in
     * _pin for LiveData value for observation.
     * @see _pin
     * @see pin
     * @param id Long: is the id of the pin to load.
     */
     fun loadPin(id: Long) {
        dataRepository.getPinByID(id, object: RepositoryCallback<Pin>{
            override fun onSuccess(data: Pin?) {
                data?.let {
                    _pin.value = data
                    fetchPlaceDetail(it.place_id)
                }
            }
            override fun onError(t: Throwable) {}
        })
    }

    fun removeEmoticon(pinId: Long) {
            dataRepository.removePinEmoticon(pinId, object: RepositoryCallback<Void>{
                override fun onSuccess(data: Void?) {
                    _emoticon.value = null
                }
                override fun onError(t: Throwable) {}
            })
    }

    fun fetchPlaceDetail(placeId: String) {
        googlePlace.detail(placeId, object: RepositoryCallback<PlaceDetail>{
            override fun onSuccess(data: PlaceDetail?) {
                _pinDetail.postValue(data)
            }
            override fun onError(t: Throwable) {
                Timber.e(t)
            }
        })
    }

}