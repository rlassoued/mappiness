package fr.mappiness.ui

import android.Manifest
import android.annotation.SuppressLint
import android.content.Intent
import android.content.IntentSender
import android.os.Build
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SwitchCompat
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationCallback
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationResult
import com.google.android.gms.location.LocationServices
import com.google.android.gms.location.LocationSettingsRequest
import com.google.android.gms.location.Priority
import com.google.android.material.navigation.NavigationBarView
import com.google.firebase.auth.FirebaseAuth
import fr.mappiness.R
import fr.mappiness.data.Pin
import fr.mappiness.databinding.ActivityHomeBinding
import fr.mappiness.notification.NotificationForegroundService
import fr.mappiness.ui.geobooks.GeobookDetailActivity.Companion.ARG_GEOBOOK
import fr.mappiness.ui.geobooks.ListGeobooksFragment
import fr.mappiness.ui.list.ListPinsFragment
import fr.mappiness.ui.map.MapFragment
import fr.mappiness.ui.map.PinDetailsFragment.Companion.ARG_PIN
import fr.mappiness.ui.profile.SettingsFragment
import permissions.dispatcher.NeedsPermission
import permissions.dispatcher.OnNeverAskAgain
import permissions.dispatcher.OnPermissionDenied
import permissions.dispatcher.OnShowRationale
import permissions.dispatcher.PermissionRequest
import permissions.dispatcher.RuntimePermissions
import timber.log.Timber

@RuntimePermissions
class HomeActivity : AppCompatActivity(), NavigationBarView.OnItemSelectedListener, NavigationBarView.OnItemReselectedListener {

    private lateinit var notificationSwitch: SwitchCompat
    private lateinit var activeFragment: androidx.fragment.app.Fragment
    private lateinit var settingsFragment: SettingsFragment
    private lateinit var mapFragment: MapFragment
    private lateinit var listFragment: ListPinsFragment
    private lateinit var listGeobooksFragment: ListGeobooksFragment

    lateinit var binding: ActivityHomeBinding

    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private val locationRequest = LocationRequest.Builder(Priority.PRIORITY_HIGH_ACCURACY, 4000).apply {
        setMinUpdateIntervalMillis(2000)
        setWaitForAccurateLocation(true)
    }.build()

    private val locationCallback = object : LocationCallback() {
        override fun onLocationResult(result: LocationResult) {
            for (location in result.locations) {
                mapFragment.onLocationChange(location)
                listFragment.onLocationChange(location)
            }
            super.onLocationResult(result)
        }
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        if (intent != null) {
            val stopService = intent.getBooleanExtra("stop", false)
            if (stopService) {
                notificationSwitch.isChecked = false
               // stopProximityNotificationService()
            }
        }
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityHomeBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setSupportActionBar(binding.myToolbar)
        supportActionBar?.setDisplayShowTitleEnabled(false)

        binding.homeActivityNavigation.setOnItemSelectedListener(this)
        binding.homeActivityNavigation.setOnItemReselectedListener(this)


        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)

        if (savedInstanceState == null) {
            settingsFragment = SettingsFragment.newInstance()
            mapFragment = MapFragment.newInstance()
            listFragment = ListPinsFragment.newInstance()
            listGeobooksFragment = ListGeobooksFragment.newInstance()
            supportFragmentManager.beginTransaction()
                .add(R.id.home_activity_content, listGeobooksFragment, ListGeobooksFragment.TAG)
                .hide(listGeobooksFragment)
                .commit()
            supportFragmentManager.beginTransaction()
                .add(R.id.home_activity_content, listFragment, ListPinsFragment.TAG)
                .hide(listFragment)
                .commit()
            supportFragmentManager.beginTransaction()
                .add(R.id.home_activity_content, settingsFragment, SettingsFragment.TAG)
                .hide(settingsFragment)
                .commit()
            supportFragmentManager.beginTransaction()
                .add(R.id.home_activity_content, mapFragment, MapFragment.TAG)
                .commit()

            activeFragment = mapFragment
        } else {
            listFragment = supportFragmentManager.findFragmentByTag(ListPinsFragment.TAG) as ListPinsFragment
            mapFragment = supportFragmentManager.findFragmentByTag(MapFragment.TAG) as MapFragment
            settingsFragment = supportFragmentManager.findFragmentByTag(SettingsFragment.TAG) as SettingsFragment
            listGeobooksFragment = supportFragmentManager.findFragmentByTag(ListGeobooksFragment.TAG) as ListGeobooksFragment
            val activeFragmentTag = savedInstanceState.getString(KEY_ACTIVE_FRAGMENT_TAG)
                ?: MapFragment.TAG
            activeFragment = supportFragmentManager.findFragmentByTag(activeFragmentTag) as androidx.fragment.app.Fragment
        }
    }

    @SuppressLint("NoDelegateOnResumeDetector")
    override fun onResume() {
        super.onResume()
        if (FirebaseAuth.getInstance().currentUser == null) {
            finish()
        }
        // Generated by permissionDispatcher
        startLocationUpdatesWithPermissionCheck()
//        mapFragment.view?.postDelayed({ intent.getParcelableExtra<Pin>(ARG_PIN)?.let { mapFragment.restoreLastScreen(it) } }, 5000)
    }

    override fun onPause() {
        super.onPause()
        stopLocationUpdates()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, intent: Intent?) = when (requestCode) {
        REQUEST_CHECK_SETTINGS -> onResultCheckSettings(resultCode)
        REQUEST_PURCHASE_GEOBOOK -> onResultPurchasedGeobook(resultCode, intent)
        else -> super.onActivityResult(requestCode, resultCode, intent)
    }

    private fun onResultCheckSettings(resultCode: Int) {
        if (resultCode == RESULT_OK) {
            Timber.d("User agreed to make required location settings changes.")
            startLocationUpdatesWithPermissionCheck()
        } else {
            Timber.d("User chose not to make required location settings changes.")
            dispatchNoLocation()
        }
    }

    private fun onResultPurchasedGeobook(resultCode: Int, intent: Intent?) {
        if (resultCode == RESULT_OK) {
            val intente = Intent(this, HomeActivity::class.java)
            intente.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
            intent?.getParcelableExtra<Pin>(ARG_PIN)?.let { intente.putExtra(ARG_PIN, it) }
            intent?.getLongExtra(ARG_GEOBOOK,-1)?.let { intente.putExtra(ARG_GEOBOOK, it) }
            startActivity(intente)
            finish()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.app_bar_actions, menu)
        val switchView = menu.findItem(R.id.enable_location_notification).actionView
        notificationSwitch = switchView!!.findViewById(R.id.notification_switch)
        notificationSwitch.isChecked = NotificationForegroundService.IS_RUNNING
        notificationSwitch.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                startProximityNotificationService()
            } else {
                stopProximityNotificationService()
            }
        }
        return true
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putString(KEY_ACTIVE_FRAGMENT_TAG, activeFragment.tag)
    }

    private fun startProximityNotificationService() {
        val intent = Intent(this, NotificationForegroundService::class.java).apply {
            putExtra(NotificationForegroundService.ARG_ACTION, NotificationForegroundService.ACTION_START)
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            startForegroundService(intent)
        } else {
            startService(intent)
        }
    }

    private fun stopProximityNotificationService() {
        stopService(Intent(this, NotificationForegroundService::class.java))
    }

    override fun onNavigationItemSelected(item: MenuItem) = when (item.itemId) {
        binding.homeActivityNavigation.selectedItemId -> true
        R.id.navigation_map -> launchFragment(mapFragment, MapFragment.TAG)
        R.id.navigation_pins_geo_books -> launchFragment(listGeobooksFragment, ListGeobooksFragment.TAG)
        R.id.navigation_pins_list -> launchFragment(listFragment, ListPinsFragment.TAG)
        R.id.navigation_profile -> launchFragment(settingsFragment, SettingsFragment.TAG)
        else -> true
    }

    fun resetFilterPinsIfApplied(){
        if(intent.hasExtra(ARG_GEOBOOK)) {
            intent.removeExtra(ARG_GEOBOOK)
            mapFragment.resetListPin()
        }
    }
    override fun onNavigationItemReselected(item: MenuItem) {
        if(item.itemId == R.id.navigation_map){
            resetFilterPinsIfApplied()
        }
    }
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        onRequestPermissionsResult(requestCode, grantResults)
    }

    private fun launchFragment(fragment: androidx.fragment.app.Fragment, tag: String): Boolean {
        supportFragmentManager.beginTransaction().hide(activeFragment).show(fragment).commit()
        activeFragment = fragment
        if(tag == MapFragment.TAG) {
            resetFilterPinsIfApplied()
        }
        return true
    }

    private fun stopLocationUpdates() {
        fusedLocationClient.removeLocationUpdates(locationCallback).addOnFailureListener {
            Timber.e(it, "Failed to remove location update")
        }
    }

    @SuppressLint("MissingPermission")
    @NeedsPermission(Manifest.permission.ACCESS_FINE_LOCATION)
    fun startLocationUpdates() {
        val builder = LocationSettingsRequest.Builder().addLocationRequest(locationRequest)
        val settingClient = LocationServices.getSettingsClient(this)
        settingClient.checkLocationSettings(builder.build()).continueWith {
            it.result
            fusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, null)
        }.addOnSuccessListener {
            Timber.d("LocationUpdates created")
        }.addOnFailureListener {
            when (it) {
                is ResolvableApiException -> try {
                    it.startResolutionForResult(this, REQUEST_CHECK_SETTINGS)
                } catch (sendEx: IntentSender.SendIntentException) {
                    Timber.e(sendEx)
                }
                else -> Timber.e(it)
            }
        }
    }

    @OnShowRationale(Manifest.permission.ACCESS_FINE_LOCATION)
    fun showRationalForLocation(request: PermissionRequest) {
        Timber.d("showRationalForLocation")
        val builder = AlertDialog.Builder(this)
        builder.setTitle(R.string.request_location_permission)
            .setMessage(R.string.request_location_explaination_message)
            .setPositiveButton(R.string.action_ok) { _, _ -> request.proceed() }
            .setNegativeButton(R.string.action_cancel) { _, _ -> request.cancel() }
        builder.create().show()
    }

    @OnPermissionDenied(Manifest.permission.ACCESS_FINE_LOCATION)
    fun onLocationDenied() {
        Timber.d("onLocationDenied")
        dispatchNoLocation()
    }

    @OnNeverAskAgain(Manifest.permission.ACCESS_FINE_LOCATION)
    fun onLocationNeverAskAgain() {
        Timber.d("onLocationNeverAskAgain")
        dispatchNoLocation()
    }

    private fun dispatchNoLocation() {
        mapFragment.onNoLocation()
        listFragment.onNoLocation()
    }

    companion object {
        const val REQUEST_CHECK_SETTINGS = 0
        const val REQUEST_PURCHASE_GEOBOOK = 1
        const val KEY_ACTIVE_FRAGMENT_TAG = "active_fragment"
    }
}
