package fr.mappiness

import android.content.Context
import com.google.android.gms.location.Geofence
import java.util.ArrayList
import java.util.Arrays

/**
 * Created by loik on 16/06/2017.
 */
object SharedPrefHelper {
    private const val PREFERENCES_NAME = "session"
    private const val KEY_NOTIF_ENABLE = "notif_enable"
    private const val KEY_GEOFENCES = "monitored_geofences"
    private const val KEY_HAS_DONE_INITIAL_SYNC = "has_done_initial_sync"
    private const val KEY_LAST_SYNC = "last_sync"
    private const val KEY_LOCATION = "location"
    private const val KEY_SHOULD_DOWNLOAD_MISSING_IMAGES = "fr.mappiness.KEY_SHOULD_DOWNLOAD_MISSING_IMAGES"
    private const val KEY_GEOBOOK_ID = "geobook_id"
    private const val KEY_TOKEN = "token"
    private const val KEY_SUBSCRIPTION_ID = "subscriptionId"
    private const val KEY_DISPLAY_SHOWCASE = "display_showcase"
    private const val DEFAULT_NOTIF_ENABLE = false
    fun setNotificationEnabled(context: Context, data: Boolean) {
        val pref = context.getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE)
        val editor = pref.edit()
        editor.putBoolean(KEY_NOTIF_ENABLE, data)
        editor.apply()
    }

    fun getNotificationEnabled(context: Context): Boolean {
        val pref = context.getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE)
        return pref.getBoolean(KEY_NOTIF_ENABLE, DEFAULT_NOTIF_ENABLE)
    }

    fun saveGeofencesIds(context: Context, geofenceList: List<Geofence>) {
        val buff = StringBuilder()
        for (geofence in geofenceList) {
            buff.append(geofence.requestId).append(",")
        }
        val pref = context.getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE)
        val editor = pref.edit()
        editor.putString(KEY_GEOFENCES, buff.toString())
        editor.apply()
    }

    fun getGeofencesIds(context: Context): List<String> {
        val pref = context.getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE)
        return ArrayList(Arrays.asList(*pref.getString(KEY_GEOFENCES, "")!!.split(",".toRegex()).toTypedArray()))
    }

    fun setHasDoneInitialSync(context: Context, value: Boolean) {
        val pref = context.getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE)
        val editor = pref.edit()
        editor.putBoolean(KEY_HAS_DONE_INITIAL_SYNC, value)
        editor.apply()
    }

    fun getHasDoneInitialSync(context: Context): Boolean {
        val pref = context.getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE)
        return pref.getBoolean(KEY_HAS_DONE_INITIAL_SYNC, false)
    }

    fun setLastSync(context: Context, date: String?) {
        val pref = context.getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE)
        val editor = pref.edit()
        editor.putString(KEY_LAST_SYNC, date)
        editor.apply()
    }

    fun getLastSync(context: Context): String? {
        val pref = context.getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE)
        return pref.getString(KEY_LAST_SYNC, null)
    }

    fun setShouldDownloadMissingImages(ctx: Context, v: Boolean) {
        val pref = ctx.getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE)
        val editor = pref.edit()
        editor.putBoolean(KEY_SHOULD_DOWNLOAD_MISSING_IMAGES, v)
        editor.apply()
    }

    fun getShouldDownloadMissingImages(context: Context): Boolean {
        val pref = context.getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE)
        return pref.getBoolean(KEY_SHOULD_DOWNLOAD_MISSING_IMAGES, true)
    }

    fun setShouldShowShowCase(context: Context, boolean: Boolean) {
        val pref = context.getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE)
        val editor = pref.edit()
        editor.putBoolean(KEY_DISPLAY_SHOWCASE, boolean)
        editor.apply()
    }

    fun getShouldShowShowCase(context: Context): Boolean {
        val pref = context.getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE)
        return pref.getBoolean(KEY_DISPLAY_SHOWCASE, true)
    }
}