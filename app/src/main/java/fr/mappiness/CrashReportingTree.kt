package fr.mappiness

import android.util.Log
import com.google.firebase.crashlytics.FirebaseCrashlytics
import timber.log.Timber

/**
 * Created by loik on 17/07/2017.
 */
internal class CrashReportingTree : Timber.Tree() {
    override fun log(priority: Int, tag: String?, message: String, throwable: Throwable?) {
        if (priority == Log.VERBOSE || priority == Log.DEBUG) {
            return
        }
        if (priority == Log.INFO) {
            FirebaseCrashlytics.getInstance().log(message)
        } else {
            val t = throwable ?: Exception(message)
            FirebaseCrashlytics.getInstance().recordException(t)
        }
    }
}