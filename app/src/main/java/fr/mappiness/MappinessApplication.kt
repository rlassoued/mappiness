package fr.mappiness


import android.content.Context
import android.content.pm.PackageManager
import androidx.multidex.MultiDexApplication
import com.google.firebase.crashlytics.FirebaseCrashlytics
import com.squareup.moshi.Moshi
import fr.mappiness.GooglePlace.GooglePlace
import fr.mappiness.data.CategoryAdapter
import fr.mappiness.data.DataRepository
import fr.mappiness.data.EmoticonAdapter
import fr.mappiness.data.network.DataRepositoryImpl
import fr.mappiness.data.network.FirebaseAuthInterceptor
import fr.mappiness.data.network.MappinessAPI
import fr.mappiness.data.network.UnAuthorizeInterceptor
import fr.mappiness.ui.details.PinDetailViewModel
import fr.mappiness.ui.filter.FilterViewModel
import fr.mappiness.ui.geobooks.GeobookBilling
import fr.mappiness.ui.geobooks.GeobookDetailViewModel
import fr.mappiness.ui.geobooks.ListGeobooksViewModel
import fr.mappiness.ui.list.ListPinsViewModel
import fr.mappiness.ui.map.PinDetailsViewModel
import fr.mappiness.ui.map.PinsMapViewModel
import fr.mappiness.ui.profile.ProfileViewModel
import fr.mappiness.utils.getPackageInfoCompat
import okhttp3.Cache
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.logger.Level
import org.koin.core.qualifier.named
import org.koin.dsl.module
import org.koin.mp.KoinPlatform.startKoin
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import timber.log.Timber


/**
 * Created by loik on 15/06/2017.
 */

class MappinessApplication : MultiDexApplication() {

    val networkModule = module {
        single<Context> { this@MappinessApplication }
        single(named("A")) { provideOkhttp(get()) }
        single<Moshi> { provideMoshi() }
        single<Retrofit> { provideRetrofit(get(named("A")), get()) }
        single<MappinessAPI> { provideMappinessAPI(get()) }
        single<DataRepository> { provideDataRepository(get()) }
        single(named("B")) { provideOkhttpForGooglePlace(get()) }
        single { provideGooglePlace(get(), get(named ("B")), get())}
    }
    val appModule = module {
        single { GeobookBilling(get(), get()) }
        viewModel { ListGeobooksViewModel(get()) }
        viewModel { GeobookDetailViewModel(get(), get()) }
        viewModel { ListPinsViewModel(get()) }
        viewModel { PinDetailViewModel(get(), get()) }
        viewModel { PinsMapViewModel(get()) }
        viewModel { FilterViewModel(get()) }
        viewModel { PinDetailsViewModel(get(), get()) }
        viewModel { ProfileViewModel(get()) }
        single { provideApplication() }
    }

    override fun onCreate() {
        super.onCreate()

        Timber.plant(if (BuildConfig.DEBUG) Timber.DebugTree() else CrashReportingTree())

        startKoin(
            listOf(
                networkModule,
                appModule
            ), if (BuildConfig.DEBUG) Level.DEBUG else Level.ERROR
        )
        FirebaseCrashlytics.getInstance().setCrashlyticsCollectionEnabled(true)
    }

    fun provideApplication() = this

    fun provideMappinessAPI(retrofit: Retrofit) = retrofit.create(MappinessAPI::class.java)

    fun provideRetrofit(client: OkHttpClient, moshi: Moshi) = Retrofit.Builder()
        .client(client)
        .addConverterFactory(MoshiConverterFactory.create(moshi))
        .baseUrl(BuildConfig.API_ENDPOINT)
        .build()

    fun provideOkhttp(context: Context) =
        OkHttpClient.Builder()
            .addInterceptor(FirebaseAuthInterceptor(context))
            .addInterceptor(UnAuthorizeInterceptor(context))
            .addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
            //.cache(Cache(cacheDir, 50 * 1024 * 1024)) // 50mb
            .build()

    fun provideMoshi() = Moshi.Builder()
        .add(EmoticonAdapter())
        .add(CategoryAdapter())
        .build()

    fun provideDataRepository(mappinessAPI: MappinessAPI) =
        DataRepositoryImpl(mappinessAPI)

    fun provideOkhttpForGooglePlace(context: Context) = OkHttpClient.Builder()
        .addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
        .cache(Cache(context.cacheDir, 50 * 1024 * 1024)) // 50mb
        .build()

    fun provideGooglePlace(context: Context, client: OkHttpClient, moshi: Moshi): GooglePlace {
        val apiKey: String = try {
            context.packageManager
                .getPackageInfoCompat(context.packageName, PackageManager.GET_META_DATA)
                .applicationInfo.metaData.getString("com.google.android.geo.API_KEY")
        } catch (e: Exception) {
            null
        } ?: ""
        return GooglePlace(apiKey, client, moshi)
    }
}
