package fr.mappiness.utils

import android.app.PendingIntent
import android.content.Context
import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.net.ConnectivityManager
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.Geofence
import com.google.android.gms.location.LocationCallback
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationResult
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapView
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import com.google.android.gms.tasks.Task
import fr.mappiness.BuildConfig
import fr.mappiness.data.Pin
import timber.log.Timber
import java.util.Locale
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException
import kotlin.coroutines.suspendCoroutine


/**
 * Extend at LatLngBound by given ratio
 */
fun LatLngBounds.extends(ratio: Int): LatLngBounds {
    val extendedLongitude = (Math.abs(this.northeast.longitude - this.southwest.longitude) / ratio)
    val extendedLatitude = (Math.abs((this.northeast.latitude - this.southwest.latitude)) / ratio)

    // Longitudes
    val topRightLongitude = this.northeast.longitude + extendedLongitude

    val bottomLeftLongitude = this.southwest.longitude - extendedLongitude

    // latitudes
    val topRightLatitude = this.northeast.latitude + extendedLatitude

    val bottomLeftLatitude = this.southwest.latitude - extendedLatitude

    val topRight = LatLng(topRightLatitude, topRightLongitude)

    val bottomLeft = LatLng(bottomLeftLatitude, bottomLeftLongitude)

    return LatLngBounds(bottomLeft, topRight)
}

fun LatLngBounds.radius() =
    6371 * Math.acos(Math.sin(center.latitude.radians()) * Math.sin(northeast.latitude.radians()) +
        Math.cos(center.latitude.radians()) * Math.cos(northeast.latitude.radians()) * Math.cos(northeast.longitude.radians() - center.longitude.radians()))

fun Double.radians() = Math.toRadians(this)

fun AppCompatActivity.isConnected(): Boolean {
    val connected = (this.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager)
        .activeNetworkInfo?.isConnected
    Timber.d("Connection status %b", connected)
    return connected ?: false
}

fun androidx.fragment.app.Fragment.isConnected(): Boolean {
    val connected = (this.activity?.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager)
        .activeNetworkInfo?.isConnected
    Timber.d("Connection status %b", connected)
    return connected ?: false
}

/**
 * Google map getMapAsync with coroutines
 */
suspend fun MapView.getMap(): GoogleMap = suspendCoroutine {
    this.getMapAsync { googleMap ->
        it.resume(googleMap)
    }
}

/**
 *  Use Google API task with coroutines
 */
@JvmName("awaitVoid")
suspend fun Task<Void>.await() = suspendCoroutine<Unit> { continuation ->
    addOnSuccessListener { continuation.resume(Unit) }
    addOnFailureListener { continuation.resumeWithException(it) }
}

suspend fun <TResult> Task<TResult>.await() = suspendCoroutine<TResult> { continuation ->
    addOnSuccessListener { continuation.resume(it) }
    addOnFailureListener { continuation.resumeWithException(it) }
}
fun PackageManager.getPackageInfoCompat(packageName: String, flags: Int = 0): PackageInfo =
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
        getPackageInfo(packageName, PackageManager.PackageInfoFlags.of(flags.toLong()))
    } else {
        @Suppress("DEPRECATION") getPackageInfo(packageName, flags)
    }
 fun getFlags(pendingIntentFlag: Int): Int {
    return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
        PendingIntent.FLAG_MUTABLE or pendingIntentFlag
    } else {
        pendingIntentFlag
    }
}
suspend fun FusedLocationProviderClient.getCurrentLocation(
    locationRequest: LocationRequest
): LocationResult? = suspendCoroutine {
    val locationCallback = object : LocationCallback() {
        override fun onLocationResult(locationResult: LocationResult) {
            removeLocationUpdates(this)
            it.resume(locationResult)
        }
    }
    try {
        requestLocationUpdates(locationRequest, locationCallback, null)
    } catch (ex: SecurityException) {
        it.resumeWithException(ex)
    }
}

fun Pin.toGeofence() = Geofence.Builder()
    .setRequestId(this.id.toString())
    .setCircularRegion(
        this.latitude,
        this.longitude,
        BuildConfig.TRIGGERING_RADIUS
    )
    .setExpirationDuration(Geofence.NEVER_EXPIRE)
    .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER)
    .setNotificationResponsiveness(BuildConfig.NOTIFICATION_RESPONSIVENESS)
    .build()

fun Pin.notificationID() = 3 + this.id.toInt()