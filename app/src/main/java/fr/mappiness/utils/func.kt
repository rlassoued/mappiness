package fr.mappiness.utils

inline fun consume(f: () -> Unit): Boolean {
    f()
    return true
}

fun displayFormatedPrice(price: Double?): String {
    if (price == null) {
        return ""
    }
    return if (price == kotlin.math.floor(price)) price.toInt().toString() else price.toString()
}