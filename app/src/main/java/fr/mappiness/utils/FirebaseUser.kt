package fr.mappiness.utils

import com.google.firebase.auth.FirebaseUser

fun FirebaseUser.currentProviderId() = this.providerData[1].providerId