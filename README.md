# Mappiness

You need a `Android/keystore.properties` file in order to build the project.
This file define the signing configuration credentials and follow this form:
```
storePassword=xxx
keyPassword=xxxx
keyAlias=xxxx
storeFile=xxxx
```
